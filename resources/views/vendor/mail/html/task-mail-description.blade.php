@if(!empty($introLine))
<p style="text-align: center; font-size: 18px; margin-bottom: 35px; line-height: 26px; color: #014E20">
{!! $introLine !!}
</p>
@endif

@if(!empty($main))
<p style="text-align: center;color: #2F3133;font-size: 18px;margin-bottom: 10px">
{!! $main !!}
</p>
<br>
@endif
