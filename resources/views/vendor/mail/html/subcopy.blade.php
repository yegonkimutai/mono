<table class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation" style="border-top: none; padding-top: 0;">
<tr>
<td style="font-size: 15px; line-height: 26px; color: #1a2434;">
{{ Illuminate\Mail\Markdown::parse($slot) }}
</td>
</tr>
</table>
