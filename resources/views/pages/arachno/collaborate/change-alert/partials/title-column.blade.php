<div class="flex flex-col justify-center mr-3 whitespace-nowrap">
  <a class="text-primary"
     href="{{ route('collaborate.change-alerts.show', ['change_alert' => $row->id]) }}">{{ $row->title }}</a>
</div>
