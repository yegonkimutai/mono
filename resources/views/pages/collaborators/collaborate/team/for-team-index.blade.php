<x-ui.card>
  <x-collaborators.team.layout :team="$team">

    @include('pages.crud.index-table')

  </x-collaborators.team.layout>
</x-ui.card>
