<a class="text-primary cursor-pointer"
   href="{{ route('collaborate.collaborators.teams.payment-requests.outstanding', ['team' => $row->id]) }}">{{ __('payments.payment_request.view_outstanding') }}</a>
