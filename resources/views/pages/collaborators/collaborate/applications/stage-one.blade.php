<x-layouts.guest>
  <div class="mt-4 flex justify-center grow max-w-4xl w-full px-2">
    <x-ui.card class="xl:max-h-full">

      @include('general.stepper')

    </x-ui.card>
  </div>
</x-layouts.guest>
