<x-ontology.legal-domain.collaborate.layout :domain="$domain">
  <div class="no-shadow">
    @include('pages.crud.index-table')
  </div>
</x-ontology.legal-domain.collaborate.layout>
