<x-layouts.app>
  <div x-init="window.print()" class="libryo-legislation bg-white ml-3 p-3 text-sm">
    <div>{!! $html !!}</div>
  </div>
</x-layouts.app>
