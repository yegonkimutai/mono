<x-layouts.collaborate>

  <x-corpus.reference.reference-data-table :base-query="$query"
                                           :fields="['collaborate-versions']" />

</x-layouts.collaborate>
