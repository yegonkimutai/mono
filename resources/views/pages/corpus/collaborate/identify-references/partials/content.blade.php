@if ($doc)
  <x-corpus.doc.collaborate.ref-creation-doc-preview :doc="$doc" :expression="$expression" />
@endif
