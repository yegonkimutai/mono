<div class="flex items-center">
  <x-ui.icon name="ballot-check" size="10" class="mr-4 text-libryo-gray-600" />
  <div>{{ __('settings.nav.applicability') }}</div>
</div>
<div class="text-libryo-gray-500 text-sm font-base mt-2">
  {{ __('compilation.context_question.applicability_description') }}
</div>
