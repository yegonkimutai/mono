<div class="text-right">
  <a
    class="text-primary cursor-pointer"
    href="{{ route('collaborate.payments.payments.show', ['payment' => $row->id]) }}"
    target="_top"
  >{{ __('payments.payment.view_payment') }}</a>

</div>
