<x-ui.input {{ $attributes }} :value="$value" type="select" :options="$codes" name="{{ $name }}"
            label="{{ $label }}" :required="$required" />
