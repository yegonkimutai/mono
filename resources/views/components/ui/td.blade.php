<td {{ $attributes->merge(['class' => 'px-6 py-4 whitespace-normal text-sm font-medium text-libryo-gray-900']) }}>
  {{ $slot }}
</td>
