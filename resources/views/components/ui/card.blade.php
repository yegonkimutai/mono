<div {{ $attributes->merge(['class' => 'w-full bg-white py-8 px-4 rounded-lg sm:p-10']) }}>
    {{ $slot }}
</div>
