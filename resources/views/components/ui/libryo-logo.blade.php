<img {{ $attributes->merge(['class' => "mx-auto $height object-contain"]) }} src="{{ $source }}"
     alt="{{ config('app.name') }}">
