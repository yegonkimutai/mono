@props(['value'])

@if ($value)
  <svg {{ $attributes->merge(['class' => 'text-primary h-5 w-5 mr-2']) }} xmlns="http://www.w3.org/2000/svg"
       viewBox="0 0 512 512">
    <path fill="currentColor"
          d="M480 128c0 8.188-3.125 16.38-9.375 22.62l-256 256C208.4 412.9 200.2 416 192 416s-16.38-3.125-22.62-9.375l-128-128C35.13 272.4 32 264.2 32 256c0-18.28 14.95-32 32-32 8.188 0 16.38 3.125 22.62 9.375L192 338.8l233.4-233.4c6.2-6.27 14.4-9.4 22.6-9.4 17.1 0 32 13.7 32 32z" />
  </svg>
@else
  <svg {{ $attributes->merge(['class' => 'text-negative h-5 w-5 mr-2']) }} xmlns="http://www.w3.org/2000/svg"
       viewBox="0 0 320 512">
    <path fill="currentColor"
          d="M310.6 361.4c12.5 12.5 12.5 32.75 0 45.25-6.2 6.25-14.4 9.35-22.6 9.35s-16.38-3.125-22.62-9.375L160 301.3 54.63 406.6C48.38 412.9 40.19 416 32 416s-16.37-3.1-22.625-9.4c-12.5-12.5-12.5-32.75 0-45.25l105.4-105.4L9.375 150.6c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 210.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-105.4 105.4L310.6 361.4z" />
  </svg>
@endif
