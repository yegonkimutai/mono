<input
  {{ $attributes->merge(['class' => 'table-action-checkbox mr-2 h-4 w-4 text-primary focus:ring-primary border-libryo-gray-300 rounded']) }}
  type="checkbox"
>
