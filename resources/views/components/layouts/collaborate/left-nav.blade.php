<div class="mx-auto grid grid-cols-1 gap-5 lg:grid-flow-col-dense lg:grid-cols-5">
  <div class="lg:col-span-1">
    {{ $left }}
  </div>
  <div class="lg:col-span-4">
    {{ $slot }}
  </div>
</div>
