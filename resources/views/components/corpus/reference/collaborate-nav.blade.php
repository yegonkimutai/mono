<x-layouts.collaborate.left-nav>
  <x-slot name="left">
    <x-ui.collaborate.sub-nav :items="$items" />
  </x-slot>
  {{ $slot }}
</x-layouts.collaborate.left-nav>
