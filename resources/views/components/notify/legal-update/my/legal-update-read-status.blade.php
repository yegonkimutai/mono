<div class="inline-block text-white text-xs py-1 px-2 rounded bg-{{ $color }} flex flex-row items-center">
  <x-ui.icon name="clock" class="hidden md:flex mr-2" />
  {{ $slot }}
</div>
