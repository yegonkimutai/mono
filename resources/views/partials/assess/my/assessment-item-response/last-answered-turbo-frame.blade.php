<turbo-frame id="assessment-item-response-last-answered-{{ $response->id }}-{{ $byOrAt }}">
  @include('partials.assess.my.assessment-item-response.last-answered')
</turbo-frame>
