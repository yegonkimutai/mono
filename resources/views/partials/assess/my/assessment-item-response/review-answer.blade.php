<div class="text-libryo-gray-600 flex flex-col items-center">
  <div class="whitespace-nowrap">
    <x-ui.timestamp :timestamp="$response->next_due_at" />
  </div>
  <div></div>
</div>
