<turbo-frame id="assessment-item-response-review-answer-{{ $response->id }}-{{ $byOrAt }}">
  @include('partials.assess.my.assessment-item-response.review-answer')
</turbo-frame>
