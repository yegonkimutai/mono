<div>
  <div>
    @include('partials.assess.assessment-item.risk-rating-description', ['assessmentItem' => $assessmentItem])
  </div>
  <div>
    @include('partials.assess.assessment-item.domain-breadcrumbs', ['assessmentItem' => $assessmentItem])
  </div>
</div>
