<div>
  <x-ui.button styling="flat" theme="primary" type="link"
               :href="route('my.tasks.task-projects.edit', ['project' => $project->hash_id])">{{ __('actions.edit') }}
  </x-ui.button>
</div>
