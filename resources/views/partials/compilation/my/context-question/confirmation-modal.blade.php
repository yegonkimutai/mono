<div class="">
  <div class="py-4">{{ __('compilation.context_question.multiple_categories') }}</div>
  <div>
    <x-ui.label for="context_hide_duplicate_notice">
      <span class="flex items-center">
        <x-ui.input label="" name="context_hide_duplicate_notice" type="checkbox"/>
        <span class="font-normal">{{ __('actions.dont_show_again') }}</span>
      </span>
    </x-ui.label>
  </div>
</div>
