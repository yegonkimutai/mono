
<x-corpus.reference.reference-bulk-action
  :expression-id="$expression->id"
  :label-suffix="$labelSuffix"
  :action="$action"
  :is-open="true"
  :check-first="true"
  :check-suffix="$checkSuffix"
/>
