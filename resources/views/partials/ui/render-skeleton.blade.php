<x-ui.skeleton :rows="$rows ?? 1" :no-circle="$noCircle ?? false" :flat="$flat ?? false" class="{{ $classes ?? '' }}" />
