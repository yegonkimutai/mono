<div class="text-center text-libryo-gray-400 pb-2 pt-8 text-xs">
  &copy; 2016 to {{ date('Y') }}, Libryo Ltd, all rights reserved.
</div>
