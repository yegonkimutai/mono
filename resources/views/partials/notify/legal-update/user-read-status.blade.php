<div class="inline-block">
  <x-notify.legal-update.my.legal-update-read-status :user="$user">
    {{ $user->fullName }}
  </x-notify.legal-update.my.legal-update-read-status>
</div>
