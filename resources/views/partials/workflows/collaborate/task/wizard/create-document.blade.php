<div>
  <x-ui.input
    name="title"
    required
    :value="old('title')"
    :label="__('collaborators.group.title')"
  />
</div>
