<div class="md:grid grid-cols-4 lg:grid-cols-5 gap-4 pt-8">
  <div class="col-span-2 lg:col-span-3 border-r border-libryo-gray-100 text-libryo-gray-600">
    @include('partials.actions.my.action-area.tasks.task-details-subrow')
  </div>

  <div class="pr-4 pb-4 pt-2 col-span-2 pl-4">
    @include('partials.actions.my.action-area.tasks.task-comments-subrow')
  </div>
</div>

