<td class="text-center pr-4" {!! $attributes !!}>
  <div class="flex justify-center">
    <x-tasks.task-type :type="$row->taskable_type" />
  </div>
</td>
