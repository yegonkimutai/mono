<div class="lg:grid grid-cols-5 gap-4">
  <div class="col-span-2 border-r border-libryo-gray-100 text-libryo-gray-600">
    @include('partials.actions.my.action-area.references.suggested-tasks')
  </div>

  <div class="pr-4 pb-4 pt-2 col-span-3">
    @include('partials.actions.my.action-area.references.requirement-details')
  </div>
</div>
