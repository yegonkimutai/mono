<turbo-stream action="{{ $action ?? 'update' }}" target="{{ $target }}">
  <template>
    {!! $partial !!}
  </template>
</turbo-stream>
