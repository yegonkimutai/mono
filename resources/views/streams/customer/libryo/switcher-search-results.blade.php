<turbo-stream action="update" target="libryo-switcher-search-results">
  <template>
    @include('partials.customer.my.libryo.switcher-list', ['listItems' => $listItems])
  </template>
</turbo-stream>
