<turbo-stream action="update" target="organisation-switcher-search-results">
  <template>
    @include('partials.customer.my.organisation.switcher-list', ['listItems' => $listItems])
  </template>
</turbo-stream>
