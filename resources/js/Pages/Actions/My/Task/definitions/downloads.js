import TableDownload from '@/vue/components/data-table/classes/TableDownload';

export default [
  new TableDownload({
    type: 'excel',
    icon: 'file-excel',
  }),
];
