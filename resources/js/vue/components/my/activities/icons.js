export const DefaultIcon = 'exchange';

export const ActivityIcons = {
  statusChange: 'exchange',
  madeComment: 'comment-lines',
  documentUpload: 'cloud-upload',
  assigneeChange: 'user-edit',
  watchTask: 'eye',
  dueDateChanged: 'calendar-edit',
  setReminder: 'alarm-clock',
  priorityChanged: 'thermometer-half',
  titleChanged: 'pencil',
};
