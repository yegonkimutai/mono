export default [
  'jpg',
  'jpeg',
  'jpe',
  'png',
  'pdf',
  'xls',
  'xlm',
  'xla',
  'xlc',
  'xlt',
  'xlw',
  'xll',
  'xld',
  'xlsm',
  'doc',
  'dot',
  'ppt',
  'pps',
  'pot',
  'ppz',
  'pptx',
  'vsd',
  'vst',
  'vss',
  'vsw',
  'xlsx',
  'docx',
  'zip',
  'zipx',
  'bin',
  'dms',
  'lrf',
  'mar',
  'so',
  'dist',
  'distz',
  'pkg',
  'bpk',
  'dump',
  'elc',
  'deploy',
  'exe',
  'dll',
  'deb',
  'dmg',
  'iso',
  'img',
  'msi',
  'msp',
  'msm',
  'buffer',
  'rar',
  'txt',
  'text',
  'conf',
  'def',
  'list',
  'log',
  'in',
  'ini',
  'asc',
  'image/jpeg',
  'image/png',
  'application/pdf',
  'application/vnd.ms-excel',
  'application/vnd.ms-excel.sheet.macroenabled',
  'application/vnd.ms-excel.sheet.macroenabled.12',
  'application/msword',
  'application/vnd.ms-powerpoint',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'application/vnd.visio',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/zip',
  'application/octet-stream',
  'application/x-rar-compressed',
  'text/plain',
];
