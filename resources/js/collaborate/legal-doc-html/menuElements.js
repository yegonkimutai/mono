export const inlineElements = [];

export const levelElements = [
  {
    text: '--  None --',
    value: 'none',
  },
  {
    text: 'Part',
    value: 'part',
  },
  {
    text: 'Chapter',
    value: 'chapter',
  },
  {
    text: 'Section',
    value: 'section',
  },
  {
    text: 'Subsection',
    value: 'subsection',
  },
  {
    text: 'Article',
    value: 'article',
  },
  {
    text: 'Paragraph',
    value: 'paragraph',
  },
  {
    text: 'Subparagraph',
    value: 'subparagraph',
  },
  {
    text: 'Note',
    value: 'note',
  },
  {
    text: 'Preamble',
    value: 'preamble',
  },
  {
    text: 'Definition',
    value: 'definition',
  },
  {
    text: 'Point',
    value: 'point',
  },
  {
    text: 'Book',
    value: 'book',
  },
  {
    text: 'Title',
    value: 'title',
  },
  {
    text: 'Subtitle',
    value: 'subtitle',
  },
  {
    text: 'Clause',
    value: 'clause',
  },
  {
    text: 'Sub-clause',
    value: 'subclause',
  },
  {
    text: 'Alinea',
    value: 'alinea',
  },
  {
    text: 'Division',
    value: 'division',
  },
  {
    text: 'Sub-division',
    value: 'subdivision',
  },
  {
    text: 'List',
    value: 'list',
  },
  {
    text: 'Sub-list',
    value: 'sublist',
  },
  {
    text: 'Rule',
    value: 'rule',
  },
  {
    text: 'Sub-rule',
    value: 'subrule',
  },
  {
    text: 'Sub-chapter',
    value: 'subchapter',
  },
  {
    text: 'Sub-part',
    value: 'subpart',
  },
  {
    text: 'Code',
    value: 'code',
  },
  {
    text: 'Schedule',
    value: 'schedule',
  },
  {
    text: 'Annex',
    value: 'annex',
  },
  {
    text: 'Provision',
    value: 'provision',
  },
  {
    text: 'Long Title',
    value: 'longTitle',
  },
];
