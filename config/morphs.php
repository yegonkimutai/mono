<?php

return [
    'commentable' => [
        'url_to_morph' => [
            'assessment-item-response' => 'assessment_item_response',
            'libryo' => 'place',
            'reference' => 'register_item',
            'update' => 'register_notification',
        ],
    ],
    'remindable' => [
        'url_to_morph' => [
            'reference' => 'register_item',
        ],
    ],
    'taskable' => [
        'url_to_morph' => [
            'assessment-item-response' => 'assessment_item_response',
            'context-question' => 'context_question',
            'libryo' => 'place',
            'reference' => 'register_item',
            'update' => 'register_notification',
        ],
    ],
];
