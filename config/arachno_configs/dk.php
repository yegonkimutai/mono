<?php

use App\Services\Arachno\Crawlers\Sources\Dk\DenmarkLovtidende;

return [
    'dk-denmark-lovtidende' => DenmarkLovtidende::class,
];
