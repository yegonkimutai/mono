<?php

use App\Services\Arachno\Crawlers\Sources\Ie\IrishStatuteBook;

return [
    'ie-irish-statute-book' => IrishStatuteBook::class,
];
