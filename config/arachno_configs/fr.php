<?php

use App\Services\Arachno\Crawlers\Sources\Fr\Legifrance;

return [
    'fr-legifrance' => Legifrance::class,
];
