<?php

use App\Services\Arachno\Crawlers\Sources\Eu\EuEurLex;

return [
    'eu-eur-lex' => EuEurLex::class,
];
