<?php

use App\Services\Arachno\Crawlers\Sources\Ni\NicaraguaAsambleaNacional;

return [
    'ni-nicaragua-asamblea-nacional' => NicaraguaAsambleaNacional::class,
];
