<?php

use App\Services\Arachno\Crawlers\Sources\TestConfig;
use App\Services\Arachno\Crawlers\Sources\TestPdfConfig;

return [
    'test' => TestConfig::class,
    'test-pdf' => TestPdfConfig::class,
];
