<?php

use App\Services\Arachno\Crawlers\Sources\Sg\SingaporeOnlineStatutes;

return [
    'sg-sso-agc' => SingaporeOnlineStatutes::class,
];
