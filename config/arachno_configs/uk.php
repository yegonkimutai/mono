<?php

use App\Services\Arachno\Crawlers\Sources\Uk\LegislationGovUk;

return [
    'uk-legislation-gov-uk' => LegislationGovUk::class,
];
