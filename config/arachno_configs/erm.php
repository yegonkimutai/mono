<?php

use App\Services\Arachno\Crawlers\Sources\ERM\Themis;

return [
    'erm-themis' => Themis::class,
];
