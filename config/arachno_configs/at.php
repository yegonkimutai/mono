<?php

use App\Services\Arachno\Crawlers\Sources\At\AustriaFederallegalInformationSystem;

return [
    'at-ris-bka-gv' => AustriaFederallegalInformationSystem::class,
];
