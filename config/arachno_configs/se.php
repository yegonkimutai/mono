<?php

use App\Services\Arachno\Crawlers\Sources\Se\SwedenRegulations;

return [
    'se-sweden-regulations' => SwedenRegulations::class,
];
