<?php

use App\Services\Arachno\Crawlers\Sources\Eg\EgyptAlamiriaUpdateEmails;

return [
    'eg-alamiria' => EgyptAlamiriaUpdateEmails::class,
];
