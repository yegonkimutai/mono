<?php

use App\Services\Arachno\Crawlers\Sources\Us\AMLegal;
use App\Services\Arachno\Crawlers\Sources\Us\Ca\CityOfSanRamon;
use App\Services\Arachno\Crawlers\Sources\Us\Ca\SanMateoLawLibrary;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerCA;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerCD;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerCO;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerFL;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerIL;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerMA;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerMD;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerMN;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerNY;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerOH;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerOR;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerPA;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerTX;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerUS;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerUSCFR;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerUT;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerVA;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerWA;
use App\Services\Arachno\Crawlers\Sources\Us\CaseMakerWI;
use App\Services\Arachno\Crawlers\Sources\Us\CodePublishing;
use App\Services\Arachno\Crawlers\Sources\Us\Ecode360Ordinances;
use App\Services\Arachno\Crawlers\Sources\Us\FederalRegister;
use App\Services\Arachno\Crawlers\Sources\Us\LegiscanBills;
use App\Services\Arachno\Crawlers\Sources\Us\Municode;
use App\Services\Arachno\Crawlers\Sources\Us\MunicodeOrdinances;
use App\Services\Arachno\Crawlers\Sources\Us\Or\ClackamasCountyCode;
use App\Services\Arachno\Crawlers\Sources\Us\Qcode;
use App\Services\Arachno\Crawlers\Sources\Us\QcodeOrdinances;
use App\Services\Arachno\Crawlers\Sources\Us\QcodeOrdinancesCustomCodes;
use App\Services\Arachno\Crawlers\Sources\Us\SandiegoCityOrdinances;
use App\Services\Arachno\Crawlers\Sources\Us\StateOfCalifornia;

return [
    'us-amlegal' => AMLegal::class,
    'us-casemaker-ca' => CaseMakerCA::class,
    'us-casemaker-cd' => CaseMakerCD::class,
    'us-casemaker-co' => CaseMakerCO::class,
    'us-casemaker-fl' => CaseMakerFL::class,
    'us-casemaker-il' => CaseMakerIL::class,
    'us-casemaker-ma' => CaseMakerMA::class,
    'us-casemaker-md' => CaseMakerMD::class,
    'us-casemaker-mn' => CaseMakerMN::class,
    'us-casemaker-ny' => CaseMakerNY::class,
    'us-casemaker-oh' => CaseMakerOH::class,
    'us-casemaker-or' => CaseMakerOR::class,
    'us-casemaker-pa' => CaseMakerPA::class,
    'us-casemaker-tx' => CaseMakerTX::class,
    'us-casemaker-us-admin' => CaseMakerUSCFR::class,
    'us-casemaker-us-stat' => CaseMakerUS::class,
    'us-casemaker-ut' => CaseMakerUT::class,
    'us-casemaker-va' => CaseMakerVA::class,
    'us-casemaker-wa' => CaseMakerWA::class,
    'us-casemaker-wi' => CaseMakerWI::class,
    'us-clackamas' => ClackamasCountyCode::class,
    'us-codebook' => CodePublishing::class,
    'us-ecode-360-ordinances' => Ecode360Ordinances::class,
    'us-federal-register' => FederalRegister::class,
    'us-law-cityofsanmateo' => SanMateoLawLibrary::class,
    'us-legiscan-bills' => LegiscanBills::class,
    'us-municode' => Municode::class,
    'us-municode-ordinances' => MunicodeOrdinances::class,
    'us-online-encodeplus' => CityOfSanRamon::class,
    'us-qcode' => Qcode::class,
    'us-qcode-ordinances' => QcodeOrdinances::class,
    'us-qcode-ordinances-custom-codes' => QcodeOrdinancesCustomCodes::class,
    'us-sandiego-city-ordinances' => SandiegoCityOrdinances::class,
    'us-state-of-california' => StateOfCalifornia::class,
];
