<?php

use App\Services\Arachno\Crawlers\Sources\Rw\RwandaLawReformCommission;

return [
    'rw-rlrc-gov' => RwandaLawReformCommission::class,
];
