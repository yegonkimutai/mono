<?php

return [
    'website_address' => 'https://collaborate.libryo.com',

    'emails' => [
        'document_validation' => [
            'collaborate.document.validation@libryo.com',
        ],
        'collaborator_applications' => [
            'collaborate.applications@libryo.com',
        ],
        'collaborator_invoice_cc' => [
            'accounts@libryo.com',
        ],
        'collaborator_invoice_bcc' => [
            'libryo@receiptbank.me',
        ],
        'team_details_change' => [
            'collaborate.task.applications@libryo.com',
        ],
        'task_applications' => [
            'collaborate.task.applications@libryo.com',
        ],
    ],

    'feedback_form_url' => 'https://forms.clickup.com/f/4axm9-1334/DS1W7A03NO5BU0BUYG',

    'get_in_touch_email' => 'libryocollaborate@libryo.com',
];
