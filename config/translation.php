<?php

return [
    'enabled' => env('TRANSLATION_ENABLED', false),
];
