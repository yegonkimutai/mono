<?php

return [
    'assess' => [
        'bulk-answer-ids' => 'assessBulkAnswerIds',
        'bulk-answer' => 'assessBulkAnswer',
    ],
    'customer' => [
        'active-libryo-mode' => 'activeLibryoMode',
        'active-libryo' => 'activeLibryo',
        'active-organisation' => 'activeOrganisation',
        'organisation-mode' => 'organisationMode',
    ],
];
