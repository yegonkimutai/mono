<?php

return [
    'location_type' => [
        'jurisdiction_types' => 'Jurisdiction Types',
    ],
    'location' => [
        'children' => 'Subjurisdictions',
        'country' => 'Country',
        'create_title' => 'Create',
        'currency' => 'Currency',
        'flag' => 'Flag',
        'index_title' => 'Jurisdictions',
        'jurisdiction' => 'Jurisdiction',
        'jurisdictions' => 'Jurisdictions',
        'level' => 'Level',
        'location_code' => 'Jurisdiction Code',
        'remove_from_file' => 'Remove from File',
        'select_jurisdictions_to_add' => 'Select jurisdictions to add to :item',
        'show_title' => 'Jurisdiction Details',
        'slug' => 'Slug',
        'title' => 'Title',
        'type' => 'Type',
    ],
];
