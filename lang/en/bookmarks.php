<?php

return [
    'bookmark' => 'Bookmark',
    'my_bookmarks' => 'My Bookmarks',
    'remove_bookmark' => 'Remove Bookmark',
    'remove_bookmark_confirmation' => 'Are you sure you want to remove this Bookmark',
];
