<?php

return [
    'country_code' => 'Country Code',
    'email' => 'Email',
    'first_name' => 'First Name',
    'job_description' => 'Job Description',
    'last_name' => 'Last Name',
    'lifecycle_stage' => 'Lifecycle Stage',
    'mobile_country_code' => 'Mobile Country Code',
    'mobile_phone' => 'Mobile Phone',
    'phone' => 'Phone (Mobile)',
    'title' => 'Title',
];
