<?php

return [
    'add_comment' => 'Add your comment...',
    'added_to_all_streams' => 'This comment was added to all applicable streams within your organisation.',
    'comment_files_count' => '{1} :value file attached|[2,*] :value files attached',
    'comment_replies_count' => '{1} :value comment reply|[2,*] :value comment replies',
    'comment' => [
        'mentioned_in_comment' => 'You were mentioned in a comment',
        'someone_replied' => 'Someone replied to one of your comments',
        'view_comment' => 'View the comment in the app',
    ],
    'comments' => 'Comments',
    'comments_added' => 'Comments Added',
    'delete_comment' => 'Delete Comment',
    'made_comment' => ':name posted a comment',
    'on' => ' on',
    'post' => 'Post',
    'posting' => 'Posting',
    'post_to_all_streams' => 'Post to all streams',
    'posting_to_all_streams' => 'Posting to all streams',
    'reply_to_comment' => 'Reply to comment',
    'collaborate' => [
        'comment' => [
            'index_title' => 'Comments',
            'viewAny' => 'View All',
            'update' => "Update Other's",
            'delete' => "Delete Other's",
        ],
    ],
    'users_reply_on' => "'s reply on",
];
