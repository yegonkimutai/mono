<?php

return [
    'hello' => 'Hello, :name',
    'nav' => [
        'actions_manager' => 'Manager',
        'actions_planner' => 'Planner',
        'all' => 'All',
        'assess' => 'Assess',
        'assessment_items' => 'Assessment Items',
        'dashboard' => 'Dashboard',
        'drives' => 'Drives',
        'files' => 'Files',
        'folders' => 'Folders',
        'legal_register' => 'Legal Register',
        'performance' => 'Performance',
        'projects' => 'Projects',
        'requirements' => 'Requirements',
        'tasks' => 'Tasks',
        'updates' => 'Updates',
    ],
];
