<?php

use App\Enums\Arachno\ConsolidationFrequency;
use App\Enums\Arachno\CrawlType;

return [
    'crawl' => [
        'not_started' => 'Not Started',
        'start_new_crawl_confirm' => 'Are you sure you want to start a new crawl?',
        'start_new_crawl' => 'Start New Crawl',
        'started_at' => 'Started At',
        'crawl_types' => [
            'crawl_type_' . CrawlType::GENERAL->value => 'General Crawl',
            'crawl_type_' . CrawlType::FULL_CATALOGUE->value => 'Full Catalogue Crawl',
            'crawl_type_' . CrawlType::NEW_CATALOGUE_WORK_DISCOVERY->value => 'New Catalogue Work Discovery Crawl',
            'crawl_type_' . CrawlType::FETCH_WORKS->value => 'Fetch Works Crawl',
            'crawl_type_' . CrawlType::WORK_CHANGES_DISCOVERY->value => 'Changes to works discovery Crawl',
            'crawl_type_' . CrawlType::FOR_UPDATES->value => 'For Updates Crawl',
            'crawl_type_' . CrawlType::SEARCH->value => 'Search Engine Crawl',
        ],
    ],
    'crawler' => [
        'can_fetch' => 'Can Fetch Works',
        'class_name' => 'Class Name',
        'create_title' => 'Create Crawler',
        'edit_title' => 'Edit Crawler',
        'enabled' => 'Enabled',
        'index_title' => 'Arachno Crawlers',
        'invalid_cron' => 'Invalid cron expression',
        'last_crawls' => 'Last Crawls',
        'needs_browser' => 'Needs Browser',
        'schedule_changes' => 'Work Changes Schedule',
        'schedule_full_catalogue' => 'Full Catalogue Crawl Schedule',
        'schedule_new_works' => 'New Catalogue Works Discovery Schedule',
        'schedule_updates' => 'For Updates/Gazettes Schedule',
        'schedule' => 'Schedule',
        'show_title' => 'View Details',
        'slug' => 'Slug',
        'source' => 'Source',
        'title' => 'Title',
    ],
    'change_alert' => [
        'changes' => 'Changes',
        'date' => 'Date Alerted',
        'identify_updates' => 'Identify Updates',
        'index_title' => 'Change Alerts',
        'previous' => 'Previous',
        'show_title' => 'Change Alert Details',
        'title' => 'Title',
        'url' => 'URL',
        'view_in_wachete' => 'View in Wachete',
        'wachete' => 'Wachete',
        'work_update' => 'Work updated: :work',
    ],
    'source_category' => [
        'source_category' => 'Source Category',
    ],
    'source' => [
        'create_title' => 'Create Source',
        'show_title' => 'Source Details',
        'consolidation_frequency' => [
            'title' => 'Consolidation Frequency',
            ConsolidationFrequency::WEEKLY->value => 'Weekly',
            ConsolidationFrequency::MONTHLY->value => 'Monthly',
            ConsolidationFrequency::QUARTERLY->value => 'Quarterly',
            ConsolidationFrequency::ANNUALLY->value => 'Annually',
        ],
        'created_at' => 'Date Created',
        'edit_title' => 'Edit Source',
        'index_title' => 'Sources',
        'ingestion' => 'Ingestion',
        'has_script' => 'Script Available',
        'hide_content' => 'Hide Document (Copyright restrictions)',
        'jurisdiction' => 'Jurisdictions',
        'jurisdictions' => 'Jurisdictions',
        'monitoring' => 'Monitoring',
        'manager' => 'Manager',
        'owner' => 'Owner',
        'permission_obtained' => 'Permission Obtained',
        'permission_notes' => 'Permission Notes',
        'primary_language' => 'Primary Language',
        'rights' => 'Rights',
        'source_content' => 'Source Content',
        'source_url' => 'Source URL',
        'source' => 'Source',
        'sources' => 'Sources',
        'title' => 'Title',
    ],
    'sources' => [
        'jurisdictions' => [
            'index_title' => 'Source Jurisdictions',
            'title' => 'Jurisdictions',
        ],
    ],
    'update_email' => [
        'date' => 'Date',
        'from' => 'From',
        'html' => 'Email Html',
        'index_title' => 'Update Emails',
        'show_title' => 'Update Email Details',
        'subject' => 'Subject',
        'text' => 'Email Text',
        'to' => 'To',
    ],
    'url_frontier_link' => [
        'anchor_text' => 'Anchor Text',
        'crawl_id' => 'Crawl ID',
        'crawler_info' => 'Crawler Information',
        'crawler_meta' => 'Crawler Meta Data',
        'crawler' => 'Crawler',
        'date_crawled' => 'Date Crawled',
        'date' => 'Date Queued',
        'for_doc' => 'For doc',
        'index_title' => 'Arachno Frontier',
        'link_info' => 'Link Information',
        'log' => 'Log',
        'needs_browser' => 'Needs Browser',
        'referer_text' => 'Referer Text',
        'referer' => 'Referer',
        'show_title' => 'Frontier Link',
        'source' => 'Source',
        'url_link_count' => '{1} :value link|[2,*] :value links',
        'url' => 'URL',
    ],
];
