<?php

return [
    'greeting' => 'Hey :name',
    'help' => 'Help',
    'lets_help_you' => 'Let\'s help you find what you\'re looking for',
    'need_training_help' => 'Want to become an expert at using Libryo, or just get a refresher on how the platform works? Click the button below to sign up for one of our online training sessions.',
    'need_training_sign_up' => 'Sign up for Training',
    'need_training' => 'Need training?',
    'search_success_help_text' => 'Our Success Portal hosts a variety of support articles which are easily searchable to help you find the answer you\'re looking for, quickly and efficiently!',
    'search_success_portal' => 'Search our Success Portal',
    'suggested_article_link_text' => [
        'assess' => 'Libryo Assess - Learn how to assess your company\'s compliance using Libryo Assess - your very own self-assessment tool',
        'corpus' => 'Your Requirements',
        'dashboard' => 'Understanding Your Dashboard',
        'drives' => 'Diving into Your Drives - Find out how to easily store and access your organisation and site-specific documents.',
        'notify' => 'Your Legal Updates - Find out how to stay up-to-date with your changing laws from Libryo.',
        'tasks' => 'How do I set a Task on Libryo? The Libryo Tasks module helps you keep track of your to-do\'s. Learn how to set and manage tasks for you or a team member!',
    ],
    'suggested_article_help' => 'Help: Suggested knowledge base article',
    'suggested_article' => 'Suggested Article',
    'want_to_chat_help' => 'Click on the button below to start chatting with one of our success agents. Please provide some detail and context to your question so that we can best assist you.',
    'want_to_chat_start_chatting' => 'Start Chatting',
    'want_to_chat' => 'Want to chat with us?',
];
