<?php

return [
    'payment_request' => [
        'confirm_create' => 'Are you sure you want to generate a payment request for this task?',
        'id' => 'ID',
        'index_title' => 'Outstanding Payments',
        'make_for_task' => 'Make payment request',
        'outstanding' => 'Outstanding payment request',
        'paid' => 'Paid',
        'task' => 'Task',
        'teams_with_outstanding' => 'Teams with outstanding payment requests',
        'units' => 'Units',
        'view_outstanding' => 'View outstanding payment requests',
    ],
    'payment' => [
        'amount' => 'Amount',
        'confirm_pay_outstanding' => 'Are you sure you want to pay all outstanding requests?',
        'currency' => 'Currency',
        'date' => 'Date Paid',
        'download_invoice' => 'Download Invoice',
        'index_title' => 'Payments',
        'pay_outstanding' => 'Pay Outstanding Requests',
        'show_title' => 'Payment Details',
        'successfully_made' => 'Payment was made successfully',
        'team' => 'Team',
        'view_payment' => 'View Payment Details',
    ],
    'mail' => [
        'thanks_for_completing_tasks' => 'Thanks for completing tasks on Libryo Collaborate this month, and for helping Libryo make law more accessible.',
        'give_feedback' => 'Lastly, please take a few minutes to <a href=":url">give us your feedback</a> about the Libryo Collaborate programme. This will help us to learn and continually improve your user experience.',
        'many_thanks' => 'Many thanks',
        'view_detailed_breakdown' => 'To view a detailed breakdown of your payment, log in to Libryo Collaborate, click on your name (top right) and then choose Billing from the dropdown. Please also find your paid invoice attached. Payment will be made on or about the 5th of the following month.',
        'view_task_ratings' => 'You can view task ratings and comments from your profile. Remember, the best way to improve is to click into the task and view the edited work.',
    ],
];
