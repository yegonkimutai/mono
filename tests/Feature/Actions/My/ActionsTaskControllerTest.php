<?php

namespace Feature\Actions\My;

use App\Enums\System\LibryoModule;
use App\Models\Tasks\Task;
use Tests\Feature\My\MyTestCase;

class ActionsTaskControllerTest extends MyTestCase
{
    public function testListingAndViewing(): void
    {
        /** @var \App\Models\Customer\Libryo $libryo */
        /** @var \App\Models\Customer\Organisation $org */
        [$user, $libryo, $org] = $this->initUserLibryoOrg();

        $libryo->enableModule(LibryoModule::actions());

        $this->get(route('my.actions.tasks.index', ['view' => 'list']))
            ->assertSuccessful()
            ->assertSee('my-libryo');
        $this->get(route('my.actions.tasks.index', ['view' => 'calendar']))
            ->assertSuccessful()
            ->assertSee('Calendar View');
    }

    public function testViewingDetails(): void
    {
        /** @var \App\Models\Customer\Libryo $libryo */
        /** @var \App\Models\Customer\Organisation $org */
        [$user, $libryo, $org] = $this->initUserLibryoOrg();

        $libryo->enableModule(LibryoModule::actions());

        $task = Task::factory()->create(['place_id' => $libryo->id]);

        $this->get(route('my.actions.tasks.show', ['task' => $task->hash_id]))
            ->assertSuccessful()
            ->assertSee($task->title);
    }
}
