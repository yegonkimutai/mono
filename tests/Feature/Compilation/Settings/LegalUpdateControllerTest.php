<?php

namespace Tests\Feature\Compilation\Settings;

use App\Enums\Notify\LegalUpdatePublishedStatus;
use App\Models\Compilation\Library;
use App\Models\Corpus\Reference;
use App\Models\Customer\Libryo;
use App\Models\Notify\LegalUpdate;
use App\Models\Notify\Pivots\LegalUpdateLibrary;
use App\Models\Notify\Pivots\LegalUpdateLibryo;
use Tests\Feature\Settings\SettingsTestCase;

class LegalUpdateControllerTest extends SettingsTestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function itShowsLegalUpdates(): void
    {
        $user = $this->signIn();
        $updates = LegalUpdate::factory()->count(3)->create();
        $route = route('my.settings.legal-updates.index', ['publish-status' => LegalUpdatePublishedStatus::UNPUBLISHED->value]);

        $this->assertForbiddenForNonAdmin($route, 'get', $user);

        $this->mySuperUser($user);

        $response = $this->get($route)->assertSuccessful();

        $updates->each(fn ($update) => $response->assertSee($update->title));
    }

    /**
     * @test
     *
     * @return void
     */
    public function itShowsLegalUpdateDetails(): void
    {
        $user = $this->signIn();
        $update = LegalUpdate::factory()->create();

        $route = route('my.settings.legal-updates.show', ['update' => $update->id]);

        $this->assertForbiddenForNonAdmin($route, 'get', $user);

        $this->mySuperUser($user);

        $this->get($route)
            ->assertSuccessful()
            ->assertSee($update->title);
    }

    /**
     * @test
     *
     * @return void
     */
    public function itAddsLibrariesViaSelection(): void
    {
        $user = $this->signIn();
        $update = LegalUpdate::factory()->create();
        $library = Library::factory()->create();

        $route = route('my.settings.compilation.libraries.session.add', ['key' => 'notify_updates']);
        $this->assertForbiddenForNonAdmin($route, 'post', $user);

        $this->mySuperUser($user);

        $this->withExceptionHandling()
            ->followingRedirects()
            ->post($route, ['library_id' => $library->id])
            ->assertSuccessful()
            ->assertSessionHas('libraries_loaded_notify_updates', [$library->id]);

        $this->followingRedirects()
            ->post(route('my.settings.legal-updates.libraries.store', $update))
            ->assertSuccessful()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseHas(new LegalUpdateLibrary(), [
            'register_notification_id' => $update->id,
            'library_id' => $library->id,
        ]);

        $library = Library::factory()->has(Reference::factory())->create();

        $reference = $library->references()->first();

        $this->assertDatabaseMissing(new LegalUpdateLibrary(), [
            'register_notification_id' => $update->id,
            'library_id' => $library->id,
        ]);

        $this->followingRedirects()
            ->post(route('my.settings.legal-updates.references.store', $update), ['references' => [$reference->id]])
            ->assertSuccessful()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseHas(new LegalUpdateLibrary(), [
            'register_notification_id' => $update->id,
            'library_id' => $library->id,
        ]);

        $update = LegalUpdate::factory()->create();
        $libryo = Libryo::factory()->create();

        $this->assertDatabaseMissing(LegalUpdateLibryo::class, [
            'register_notification_id' => $update->id,
            'place_id' => $libryo->id,
        ]);

        $this->followingRedirects()
            ->post(route('my.settings.legal-updates.libryos.store', $update), ['libryos' => [$libryo->id]])
            ->assertSuccessful()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseHas(LegalUpdateLibryo::class, [
            'register_notification_id' => $update->id,
            'place_id' => $libryo->id,
        ]);
    }

    /**
     * @test
     *
     * @return void
     */
    public function itRemovesFromUpdate(): void
    {
        $user = $this->signIn();
        $update = LegalUpdate::factory()->create();
        $library = Library::factory()->create();
        $update->libraries()->attach($library->id);

        $route = route('my.settings.legal-updates.actions', ['update' => $update->id]);
        $this->assertForbiddenForNonAdmin($route, 'post', $user);

        $this->mySuperUser($user);

        $payload = [
            'action' => 'remove_from_update',
            "actions-checkbox-{$library->id}" => true,
        ];

        $this->assertDatabaseHas(new LegalUpdateLibrary(), [
            'register_notification_id' => $update->id,
            'library_id' => $library->id,
        ]);

        $this->followingRedirects()
            ->post($route, $payload)
            ->assertSuccessful()
            ->assertSessionHasNoErrors();

        $this->assertDatabaseMissing(new LegalUpdateLibrary(), [
            'register_notification_id' => $update->id,
            'library_id' => $library->id,
        ]);
    }
}
