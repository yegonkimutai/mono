<?php

namespace Tests\Feature\Compilation\My;

use App\Enums\Compilation\ApplicabilityActivityType;
use App\Enums\Corpus\ReferenceType;
use App\Models\Assess\AssessmentItem;
use App\Models\Auth\User;
use App\Models\Compilation\ApplicabilityActivity;
use App\Models\Compilation\ContextQuestion;
use App\Models\Corpus\Reference;
use App\Models\Customer\Libryo;
use App\Models\Customer\Organisation;
use App\Models\Geonames\Location;
use App\Models\Ontology\LegalDomain;
use Tests\Feature\Settings\SettingsTestCase;

class ApplicabilityStreamControllerTest extends SettingsTestCase
{
    protected function createBase(): array
    {
        $user = $this->signIn();
        $org = Organisation::factory()->create();
        $libryo = Libryo::factory()->for($org)->create();
        $question = ContextQuestion::factory()->create();

        $user->libryos()->attach($libryo->id);
        $user->organisations()->attach($org, ['is_admin' => true]);
        $question->libryos()->attach($libryo->id);

        $this->activateAllStreams($user);

        return [$org, $libryo, $question, $user];
    }

    public function testFetchingReferencesAndAssessments(): void
    {
        /** @var Libryo $libryo */
        [$org, $libryo, $question] = $this->createBase();
        $location = Location::factory()->create();
        $libryo->update(['location_id' => $location->id]);
        $libryo->updateSetting('use_legal_domains', true);
        $libryo->updateSetting('use_collections', true);

        $domain = LegalDomain::factory()->create();
        $domain->update(['top_parent_id' => $domain->id]);
        $libryo->legalDomains()->attach($domain->id);

        /** @var Reference $reference */
        $nonLibryoReference = Reference::factory()->create(['type' => ReferenceType::citation()->value]);
        $reference = Reference::factory()->create(['type' => ReferenceType::citation()->value]);
        $reference->locations()->attach($location->id);
        $reference->legalDomains()->attach($domain->id);

        $question->references()->attach([$nonLibryoReference->id, $reference->id]);

        $this->get(route('my.references.for.context-questions.index', ['question' => $question->id, 'libryo' => $libryo->id]))
            ->assertSuccessful()
            ->assertSee($reference->refPlainText->plain_text)
            ->assertDontSee($nonLibryoReference->refPlainText->plain_text);

        $assessment = AssessmentItem::factory()->create();
        $nonLibryoAssessment = AssessmentItem::factory()->create();

        $reference->assessmentItems()->attach($assessment->id);

        $this->get(route('my.assessment-items.for.context-questions.index', ['question' => $question->id, 'libryo' => $libryo->id]))
            ->assertSuccessful()
            ->assertSee($assessment->description)
            ->assertDontSee($nonLibryoAssessment->description);
    }

    public function testFetchingActivities(): void
    {
        [$org, $libryo, $question, $user] = $this->createBase();

        $inLibryo = ApplicabilityActivity::factory()->create(['activity_type' => ApplicabilityActivityType::ANSWER_CHANGED->value, 'place_id' => $libryo->id, 'context_question_id' => $question->id, 'user_id' => $user->id]);
        $notInLibryo = ApplicabilityActivity::factory()->create(['activity_type' => ApplicabilityActivityType::ANSWER_CHANGED->value, 'context_question_id' => $question->id]);

        $nonOrgUser = User::whereKey($notInLibryo->user_id)->first();

        $this->get(route('my.activities.for.context-questions.index', ['question' => $question->id, 'libryo' => $libryo->id]))
            ->assertSuccessful()
            ->assertSee('changed the answer from')
            ->assertSee($user->full_name)
            ->assertDontSee($nonOrgUser->full_name);
    }

    public function testFetchingTasks(): void
    {
        [$org, $libryo, $question] = $this->createBase();
        $NonOrglibryo = Libryo::factory()->create();
        $task = $question->tasks()->create(['title' => 'In Libryo Task', 'place_id' => $libryo->id]);
        $nonTask = $question->tasks()->create(['title' => 'Not Libryo Task', 'place_id' => $NonOrglibryo->id]);

        $this->get(route('my.tasks.for.context-questions.index', ['question' => $question->id, 'libryo' => $libryo->id]))
            ->assertSuccessful()
            ->assertSee($task->title)
            ->assertDontSee($nonTask->title);
    }

    public function testFetchingComments(): void
    {
        [$org, $libryo, $question] = $this->createBase();
        $this->get(route('my.comments.for.context-questions.index', ['question' => $question->id, 'libryo' => $libryo->id]))
            ->assertSuccessful();
    }
}
