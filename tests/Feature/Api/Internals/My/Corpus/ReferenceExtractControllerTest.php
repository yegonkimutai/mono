<?php

namespace Tests\Feature\Api\Internals\My\Corpus;

use App\Models\Actions\ActionArea;
use App\Models\Corpus\Reference;
use App\Models\Corpus\ReferenceContentExtract;
use App\Models\Tasks\Task;
use Tests\Feature\My\MyTestCase;

class ReferenceExtractControllerTest extends MyTestCase
{
    public function testGettingExtracts(): void
    {
        [$user, $libryo, $org] = $this->initUserLibryoOrg();
        $action = ActionArea::factory()->create();
        $reference = Reference::factory()->create();
        $extracts = ReferenceContentExtract::factory()->count(3)->create(['reference_id' => $reference->id]);
        Task::factory()->create(['place_id' => $libryo->id, 'action_area_id' => $action->id, 'reference_content_extract_id' => $extracts[0]->id]);

        $this->getJson(route('api.my.references.extracts.index', ['reference' => $reference->id]))
            ->assertSuccessful()
            ->assertJson([
                'data' => $extracts->map(fn ($item, $index) => [
                    'id' => $item->id,
                    'content' => $item->content,
                    'attached' => $index === 0,
                ])->all(),
            ]);
    }
}
