<?php

namespace Tests\Feature\Corpus\Collaborate;

use App\Models\Arachno\Source;
use App\Models\Corpus\CatalogueDoc;
use App\Models\Corpus\Doc;
use Illuminate\Database\Eloquent\Collection;
use Tests\Feature\Traits\HasVisibleFieldAssertions;
use Tests\TestCase;

class DocForCatalogueDocControllerTest extends TestCase
{
    use HasVisibleFieldAssertions;

    /**
     * The list of database columns that should be visible on the pages.
     *
     * @return string[]
     */
    protected static function visibleFields(): array
    {
        return ['title'];
    }

    public function testItRendersTheIndexPage(): void
    {
        /** @var Source */
        $source = Source::factory()->create();
        $uniqueId = '12345';
        /** @var CatalogueDoc */
        $catalogueDoc = CatalogueDoc::factory()->create(['source_unique_id' => $uniqueId, 'source_id' => $source->id]);
        $catalogueDoc->setUid()->save();
        /** @var Collection<Doc> */
        $docs = Doc::factory(3)->create(['source_unique_id' => $uniqueId, 'source_id' => $source->id]);
        $docs->each(fn ($d) => $d->setUid()->save());

        $routeName = 'collaborate.corpus.catalogue-docs.docs.index';
        $route = route($routeName, ['catalogueDoc' => $catalogueDoc->id]);
        $this->validateAuthGuard($route);
        $this->collaboratorSignIn();
        $this->validateCollaborateRole($route);
        $response = $this->get($route)->assertSuccessful();
        $this->assertSeeVisibleFields($docs[0], $response);
    }
}
