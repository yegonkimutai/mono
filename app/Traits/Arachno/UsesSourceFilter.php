<?php

namespace App\Traits\Arachno;

use App\Models\Arachno\Source;
use Illuminate\View\ComponentAttributeBag;

trait UsesSourceFilter
{
    /**
     * @return array<string, mixed>
     */
    public function getSourceFilter(): array
    {
        return [
            'label' => __('arachno.source.source'),
            'render' => fn () => view('components.arachno.source.source-selector', [
                'value' => null,
                'attributes' => new ComponentAttributeBag(['@change' => '$dispatch(\'changed\', $el.value)']),
                'name' => 'source',
                'label' => '',
                'required' => false,
                'allowEmpty' => true,
            ]),
            // @phpstan-ignore-next-line
            'value' => fn ($value) => Source::find($value)?->title ?? '',
        ];
    }
}
