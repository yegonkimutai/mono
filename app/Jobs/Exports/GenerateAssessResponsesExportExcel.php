<?php

namespace App\Jobs\Exports;

use App\Contracts\System\TrackableJobInterface;
use App\Exports\Assess\AssessResponsesExcelExport;
use App\Jobs\Traits\Trackable;
use App\Models\Auth\User;
use App\Models\Customer\Libryo;
use App\Models\Customer\Organisation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateAssessResponsesExportExcel implements ShouldQueue, TrackableJobInterface
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use Trackable;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 600;

    /**
     * @param string               $tempFileName
     * @param Organisation         $organisation
     * @param User                 $user
     * @param array<string, mixed> $filters
     */
    public function __construct(
        protected string $tempFileName,
        protected User $user,
        protected Organisation $organisation,
        protected ?Libryo $libryo,
        protected array $filters = [],
    ) {
        $this->prepareStatus();
        $this->setProgressMax(100);
        $this->onQueue('exports');
    }

    /**
     * @param AssessResponsesExcelExport $exporter
     *
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     *
     * @return void
     */
    public function handle(AssessResponsesExcelExport $exporter): void
    {
        $path = config('filesystems.paths.temp') . DIRECTORY_SEPARATOR . $this->tempFileName;

        $progressCallback = function ($progress) {
            // @codeCoverageIgnoreStart
            $this->setProgressNow($progress);
            // @codeCoverageIgnoreEnd
        };

        $excel = $exporter->export(
            $this->user,
            $this->organisation,
            $this->libryo,
            $this->filters,
            $progressCallback
        );

        $writer = new Xlsx($excel);
        $localTmpPath = storage_path('app/tmp') . DIRECTORY_SEPARATOR . $this->tempFileName . '_tmp';
        if (!is_dir(storage_path('app/tmp'))) {
            // @codeCoverageIgnoreStart
            mkdir(storage_path('app/tmp'));
            // @codeCoverageIgnoreEnd
        }
        $writer->save($localTmpPath);
        /** @var string */
        $fileContents = file_get_contents($localTmpPath);
        Storage::put($path, $fileContents);
        $this->setProgressNow(100);
        unlink($localTmpPath);
    }
}
