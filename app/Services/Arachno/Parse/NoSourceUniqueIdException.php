<?php

namespace App\Services\Arachno\Parse;

use RuntimeException;

class NoSourceUniqueIdException extends RuntimeException
{
}
