<?php

namespace App\Services\Arachno\Parse;

use OutOfBoundsException;

class InvalidQueryException extends OutOfBoundsException
{
}
