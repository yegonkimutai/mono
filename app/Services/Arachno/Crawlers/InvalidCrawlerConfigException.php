<?php

namespace App\Services\Arachno\Crawlers;

use OutOfBoundsException;

class InvalidCrawlerConfigException extends OutOfBoundsException
{
}
