<?php
/*
|--------------------------------------------------------------------------
| Slug: default-spider-config
| Title: Default Spider Config
| URL: https://www.example.com
|--------------------------------------------------------------------------
*/

return [
    'capture_body_queries' => [],
    'capture_head_queries' => [],
    'capture_queries' => [],
    'css_exclude_selectors' => [],
    'css' => null,
    'default_cookies' => [],
    'effective_date_query' => null,
    'follow_links_queries' => [],
    'is_meta_page_query' => null,
    'is_toc_page_query' => null,
    'publication_document_number_query' => null,
    'publication_number_query' => null,
    'start_urls' => [],
    'throttle_requests' => null,
    'title_query' => null,
    'toc_item_criteria' => null,
    'toc_item_determine_depth' => null,
    'toc_item_extract_label' => null,
    'toc_item_extract_link' => null,
    'toc_query' => null,
    'unique_id_query' => null,
    'user_agent' => null,
    'verify_ssl' => true,
    'work_date_query' => null,
    'work_number_query' => null,
    'work_type_query' => null,
];
