<?php

namespace App\Services\Arachno\Crawlers\Sources\Ma;

use App\Enums\Arachno\CrawlType;
use App\Models\Arachno\UrlFrontierLink;
use App\Models\Corpus\CatalogueDoc;
use App\Models\Corpus\Doc;
use App\Services\Arachno\Crawlers\AbstractCrawlerConfig;
use App\Services\Arachno\Frontier\PageCrawl;
use App\Services\Arachno\Parse\DocMetaDto;
use DOMElement;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

/**
 * @codeCoverageIgnore
 *
 * slug: ma-arabe-accueil-national
 * title: Morocco Arabe Accueil National
 * url: http://www.sgg.gov.ma
 */
class MoroccoArabeAccueilNational extends AbstractCrawlerConfig
{
    private string $baseDomain = 'http://www.sgg.gov.ma';

    /**
     * The config that is implemented by the crawler.
     *
     * @return array<string, mixed>
     */
    public function getCrawlerSettings(): array
    {
        return [
            'slug' => 'ma-arabe-accueil-national',
            'throttle_requests' => 400,
            'start_urls' => $this->getArabeMoroccoStartUrls(),
        ];
    }

    /**
     * @param PageCrawl $pageCrawl
     *
     * @return void
     */
    public function parsePage(PageCrawl $pageCrawl): void
    {
        if ($this->arachno->crawlIsFullCatalogue($pageCrawl) && ($this->arachno->matchUrl($pageCrawl, '/arabe\/textesimportants\.aspx/') || $this->arachno->matchUrl($pageCrawl, '/arabe\/textesconsolides\.aspx/'))) {
            $this->handleCatalogue($pageCrawl);
        }
        if ($this->arachno->crawlIsFetchWorks($pageCrawl)) {
            if ($this->arachno->matchUrl($pageCrawl, '/\.pdf/')) {
                $this->handleMeta($pageCrawl);
                $this->arachno->capturePDF($pageCrawl);
            }
        }
        if ($this->arachno->crawlIsForUpdates($pageCrawl)) {
            if ($this->arachno->matchUrl($pageCrawl, '/Legislation\.asp/')) {
                $this->handleUpdatesForLegislations($pageCrawl);
            }
            if ($this->arachno->matchUrl($pageCrawl, '/BulletinOfficiel\.aspx/')) {
                $this->handleUpdatesForActs($pageCrawl);
            }
            if ($this->arachno->matchUrl($pageCrawl, '/\.pdf/')) {
                $this->arachno->capturePDF($pageCrawl);
            }
        }
    }

    public function preFetch(PageCrawl $pageCrawl): void
    {
        if ($this->arachno->crawlIsFullCatalogue($pageCrawl)) {
            if ($this->arachno->matchUrl($pageCrawl, '/arabe\/textesimportants\.aspx/')) {
                $pageCrawl->setProxySettings([
                    'provider' => 'scraping_bee',
                    'options' => [
                        'render_js' => true,
                        'wait_for' => 'div#Agg3120_Accordion h3#ui-id-6',
                        'js_scenario' => [
                            'instructions' => [
                                ['evaluate' => 'document.querySelectorAll("div#Agg3120_Accordion h3#ui-id-6").forEach((el) => el.click())'],
                                ['wait' => 3000],
                            ],
                        ],
                    ],
                ]);
            }
            if ($this->arachno->matchUrl($pageCrawl, '/arabe\/textesconsolides\.aspx/')) {
                $pageCrawl->setProxySettings([
                    'provider' => 'scraping_bee',
                    'options' => [
                        'render_js' => true,
                        'wait_for' => 'div#Agg3255_Accordion h3#ui-id-4',
                        'js_scenario' => [
                            'instructions' => [
                                ['evaluate' => 'document.querySelectorAll("div#Agg3255_Accordion h3#ui-id-4").forEach((el) => el.click())'],
                                ['wait' => 3000],
                            ],
                        ],
                    ],
                ]);
            }
        }
        if ($this->arachno->crawlIsForUpdates($pageCrawl) && $this->arachno->matchUrl($pageCrawl, '/BulletinOfficiel\.aspx/')) {
            $pageCrawl->setProxySettings([
                'provider' => 'scraping_bee',
                'options' => [
                    'render_js' => true,
                    'wait_browser' => 'networkidle0',
                    'wait_for' => 'table[id="2873"] tbody tr[role="row"] a[href]',
                ],
            ]);
        }
    }

    /**
     * @param PageCrawl $pageCrawl
     *
     * @return void
     */
    protected function handleCatalogue(PageCrawl $pageCrawl): void
    {
        $this->arachno->each($pageCrawl, 'div.ui-accordion-content.ui-corner-bottom.ui-helper-reset.ui-widget-content.ui-accordion-content-active ul li a[href], div.ui-accordion-content.ui-corner-bottom.ui-helper-reset.ui-widget-content.ui-accordion-content-active div p a[href]', function ($items) use ($pageCrawl) {
            $href = $items->getAttribute('href');
            $startUrl = !str_contains($href, 'sgg.gov.ma') ? $this->baseDomain . $href : $href;
            $uniqueId = Str::of($href)->afterLast('/')->before('.pdf');
            $title = $items->textContent ?? '';
            $catDoc = new CatalogueDoc([
                'title' => $title,
                'start_url' => $startUrl,
                'view_url' => $startUrl,
                'source_unique_id' => $uniqueId,
                'language_code' => 'ara',
                'primary_location_id' => 173834,
            ]);

            $this->arachno->createCatalogueDoc($pageCrawl, $catDoc);
        });
    }

    /**
     * @param PageCrawl $pageCrawl
     *
     * @return void
     */
    protected function handleUpdatesForActs(PageCrawl $pageCrawl): void
    {
        $this->arachno->each($pageCrawl, 'table[id="2873"] tbody tr[role="row"] a[href]', function ($items) use ($pageCrawl) {
            $dom = new DomCrawler($items?->parentNode?->parentNode?->parentNode);
            $link = $dom->filter('a[href]')->getNode(0);
            /** @var DOMElement $link */
            $href = $link->getAttribute('href');
            $pdfLink = str_contains($href, 'http://www.sgg.gov.ma') ? $href : $this->baseDomain . $href;
            $uniqueId = Str::of($href)->afterLast('/')->before('.pdf');
            $title = $dom->filter('td:nth-child(1)')->getNode(0)?->textContent ?? '';
            $date = $dom->filter('td:nth-child(2)')->getNode(0)?->textContent ?? '';
            $docMeta = new DocMetaDto();
            $docMeta->work_number = $uniqueId;
            $docMeta->source_unique_id = 'updates_' . $uniqueId;
            $docMeta->title = 'Bulletin Officiel ' . $title;
            $docMeta->language_code = 'ara';
            $docMeta->primary_location = '173834';
            $docMeta->work_type = 'act';
            $docMeta->work_date = Carbon::parse($date);
            $docMeta->source_url = $pdfLink;
            $l = new UrlFrontierLink(['url' => $pdfLink, 'anchor_text' => 'PDF']);
            $l->_metaDto = $docMeta;
            $this->arachno->followLink($pageCrawl, $l, true);
        });
    }

    /**
     * @param PageCrawl $pageCrawl
     *
     * @return void
     */
    protected function handleUpdatesForLegislations(PageCrawl $pageCrawl): void
    {
        $this->arachno->each($pageCrawl, 'div.TextesRegissantLeSGGContainer ul li a[href]', function (DOMElement $items) use ($pageCrawl) {
            $dom = new DomCrawler($items->parentNode);
            $link = $dom->filter('a[href]')->getNode(0);
            /** @var DOMElement $link */
            $href = $link->getAttribute('href');
            $pdfLink = str_contains($href, 'http://www.sgg.gov.ma') ? $href : $this->baseDomain . $href;
            $uniqueId = Str::of($href)->afterLast('/')->before('.pdf');
            $docMeta = new DocMetaDto();
            $docMeta->work_number = $uniqueId;
            $docMeta->source_unique_id = 'updates_' . $uniqueId;
            $docMeta->title = $link->textContent ?? '';
            $docMeta->language_code = 'ara';
            $docMeta->primary_location = '173834';
            $docMeta->work_type = 'legislation';
            $docMeta->source_url = $pdfLink;
            $l = new UrlFrontierLink(['url' => $pdfLink, 'anchor_text' => 'PDF']);
            $l->_metaDto = $docMeta;
            $this->arachno->followLink($pageCrawl, $l, true);
        });
    }

    /**
     * @param PageCrawl $pageCrawl
     *
     * @return Doc
     */
    protected function handleMeta(PageCrawl $pageCrawl): Doc
    {
        /** @var CatalogueDoc */
        $catalogueDoc = $pageCrawl->pageUrl->catalogueDoc;
        $doc = $this->arachno->setDocMetaProperty($pageCrawl, 'source_unique_id', $catalogueDoc->source_unique_id);
        $this->arachno->setDocMetaProperty($pageCrawl, 'language_code', 'ara');
        $this->arachno->setDocMetaProperty($pageCrawl, 'title_translation', $catalogueDoc->title_translation);
        $this->arachno->setDocMetaProperty($pageCrawl, 'title', $catalogueDoc->title);
        $this->arachno->setDocMetaProperty($pageCrawl, 'primary_location', 173834);
        $this->arachno->setDocMetaProperty($pageCrawl, 'work_type', (string) 'law');
        $this->arachno->setDocMetaProperty($pageCrawl, 'source_url', $catalogueDoc->start_url);
        $this->arachno->setDocMetaProperty($pageCrawl, 'download_url', $catalogueDoc->start_url);

        return $doc;
    }

    /**
     * @return array<string,array<UrlFrontierLink>>
     */
    protected function getArabeMoroccoStartUrls(): array
    {
        $timestamp = round(microtime(true) * 1000);

        return [
            'type_' . CrawlType::FULL_CATALOGUE->value => [
                new UrlFrontierLink(['url' => 'http://www.sgg.gov.ma/arabe/textesimportants.aspx']),
                new UrlFrontierLink(['url' => 'http://www.sgg.gov.ma/arabe/textesconsolides.aspx']),
            ],
            'type_' . CrawlType::FOR_UPDATES->value => [
                new UrlFrontierLink(['url' => 'http://www.sgg.gov.ma/BulletinOfficiel.aspx']),
                new UrlFrontierLink(['url' => 'http://www.sgg.gov.ma/Legislation.aspx#Agg2615_2']),
            ],
        ];
    }
}
