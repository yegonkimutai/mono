<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerNY extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'NY';
}
