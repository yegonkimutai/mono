<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerVA extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'VA';
}
