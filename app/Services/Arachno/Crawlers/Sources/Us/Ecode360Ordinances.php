<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

use App\Enums\Arachno\CrawlType;
use App\Enums\Arachno\Parse\DomQueryType;
use App\Models\Arachno\UrlFrontierLink;
use App\Models\Corpus\CatalogueDoc;
use App\Models\Corpus\Doc;
use App\Services\Arachno\Crawlers\AbstractCrawlerConfig;
use App\Services\Arachno\Frontier\LinkToUri;
use App\Services\Arachno\Frontier\PageCrawl;
use App\Services\Arachno\Parse\DocMetaDto;
use App\Services\Arachno\Parse\TocItemDraft;
use App\Services\Arachno\Support\DomClosest;
use Carbon\Exceptions\InvalidFormatException;
use DOMElement;
use DOMNode;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

/**
 * @codeCoverageIgnore
 *
 * slug: us-ecode-360-ordinances
 * title: USA Ecode360 Ordinances
 * url: https://ecode360.com
 */
class Ecode360Ordinances extends AbstractCrawlerConfig
{
    /**
     * @param PageCrawl $pageCrawl
     *
     * @return void
     */
    public function parsePage(PageCrawl $pageCrawl): void
    {
        if ($this->arachno->crawlIsFullCatalogue($pageCrawl) && $this->arachno->matchCSS($pageCrawl, 'div#contentContainer')) {
            $this->handleCatalogue($pageCrawl);
        }
        if ($this->arachno->crawlIsForUpdates($pageCrawl)) {
            if ($this->arachno->matchCSS($pageCrawl, 'body#lawsPage')) {
                $this->handleLawsPage($pageCrawl);
                // $this->getDispositionLinks($pageCrawl);
            }
            if ($this->arachno->matchUrl($pageCrawl, '/\.pdf$/')) {
                $this->arachno->capturePDF($pageCrawl);
            }
        }
        if ($this->arachno->crawlIsFetchWorks($pageCrawl)) {
            $this->handleMeta($pageCrawl);
            if ($this->arachno->matchUrl($pageCrawl, '/print\/[A-Z0-9]+\?guid=/')) {
                $this->handleCapture($pageCrawl);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function preFetch(PageCrawl $pageCrawl): void
    {
        $pageCrawl->setProxySettings([
            'provider' => 'scraping_bee',
            'options' => ['render_js' => false],
        ]);
    }

    /**
     * @param PageCrawl $pageCrawl
     *
     * @return void
     */
    protected function handleCatalogue(PageCrawl $pageCrawl): void
    {
        $url = $pageCrawl->pageUrl->url;
        $href = $pageCrawl->domCrawler->filter('a#printButton')->link()->getUri();
        $code = (string) Str::of($href)->after('guid=');
        $locations = $this->getEcode360LocationsAndTitles()[$code]['location_id'] ?? null;
        $title = $this->getEcode360LocationsAndTitles()[$code]['title'] ?? null;
        $catDoc = new CatalogueDoc([
            'title' => trim($title),
            'start_url' => $href,
            'view_url' => $url,
            'source_unique_id' => $code,
            'language_code' => 'eng',
            'primary_location_id' => $locations,
        ]);

        $this->arachno->createCatalogueDoc($pageCrawl, $catDoc);
    }

    /**
     * @param PageCrawl $pageCrawl
     *
     * @return Doc
     */
    protected function handleMeta(PageCrawl $pageCrawl): Doc
    {
        /** @var CatalogueDoc */
        $catalogueDoc = $pageCrawl->pageUrl->catalogueDoc;
        $doc = $this->arachno->setDocMetaProperty($pageCrawl, 'source_unique_id', $catalogueDoc->source_unique_id);
        $this->arachno->setDocMetaProperty($pageCrawl, 'language_code', 'eng');
        $this->arachno->setDocMetaProperty($pageCrawl, 'title', $catalogueDoc->title);
        $this->arachno->setDocMetaProperty($pageCrawl, 'primary_location', $catalogueDoc->primary_location_id);
        $this->arachno->setDocMetaProperty($pageCrawl, 'work_type', (string) 'code');
        $this->arachno->setDocMetaProperty($pageCrawl, 'source_url', $catalogueDoc->start_url);
        $this->arachno->setDocMetaProperty($pageCrawl, 'start_url', $catalogueDoc->start_url);

        return $doc;
    }

    protected function handleLawsPage(PageCrawl $pageCrawl): void
    {
        // @codeCoverageIgnoreStart
        $dom = $pageCrawl->domCrawler;
        $jurisdiction = $dom->filter('head title')->getNode(0)?->textContent ?? '';
        $jurisdiction = str_replace(' New Laws', '', $jurisdiction);
        $baseUrl = $pageCrawl->getFinalRedirectedUrl();
        $prefix = str_replace('https://ecode360.com/', '', $baseUrl);
        $prefix = Str::after($prefix, 'laws/');
        $locations = $this->getEcode360LocationsAndTitles()[$prefix];
        foreach ($dom->filter('table#newLawsTable tr .titleLawCell a') as $link) {
            /** @var DOMElement $link */
            $href = $link->getAttribute('href');
            $href = (string) app(LinkToUri::class)->convert($href, $baseUrl);
            $uniqueId = Str::of($href)->after('com/')->before('.pdf');
            $docTitle = trim($link->textContent);
            $title = implode(' - ', [$jurisdiction, $docTitle]);
            $title = Str::of($title)->replace('pdf', '');
            $docMeta = new DocMetaDto();
            // $titleNoSpaces = preg_replace(['/\s+/', '/,/'], '', $title);
            $docMeta->source_unique_id = $uniqueId;
            $docMeta->title = $title;
            $docMeta->work_type = 'ordinance';
            $docMeta->language_code = 'eng';
            $closest = app(DomClosest::class);
            /** @var DOMNode $link */
            /** @var Crawler $tr */
            $tr = $closest->closest($link, 'tr');

            $hasDate = $tr->filter('.adoptedLawCell')->count() > 0;
            try {
                if ($hasDate) {
                    $date = $tr->filter('.adoptedLawCell')->getNode(0)?->textContent ?? null;
                    // @phpstan-ignore-next-line
                    $docMeta->work_date = $date ? Carbon::createFromFormat('Y-m-d', substr(trim($date), 0, 10)) : null;
                }
            } catch (InvalidFormatException $th) {
            }

            $summary = $tr->filter('.subjectLawCell')->getNode(0)?->textContent ?? null;
            $affects = $tr->filter('.dispositionsLawCell')->getNode(0)?->textContent ?? null;
            $summary = ($summary ?? '') . ($affects ? ' Affects: ' . $affects : '');
            if ($summary) {
                $docMeta->summary = $summary;
            }
            $docMeta->work_number = $docTitle;
            $docMeta->primary_location = (string) ($locations['location_id'] ?? '');
            $docMeta->source_url = $href;
            $docMeta->download_url = $href;

            $l = new UrlFrontierLink(['url' => $href, 'anchor_text' => $title]);
            $l->_metaDto = $docMeta;
            $this->arachno->followLink($pageCrawl, $l, true);
        }
    }

    /**
     * @param PageCrawl $pageCrawl
     *
     * @return void
     */
    protected function handleCapture(PageCrawl $pageCrawl): void
    {
        $bodyQueries = $this->arrayOfQueries([
            [
                'type' => DomQueryType::CSS,
                'query' => 'div#content',
            ],
        ]);
        $headQueries = $this->arrayOfQueries([
            [
                'type' => DomQueryType::CSS,
                'query' => 'html body h1',
            ],
            [
                'type' => DomQueryType::XPATH,
                'query' => '//head//link[contains(@href,"/css") and @rel="stylesheet"]',
            ],
        ]);
        $preStore = function (Crawler $crawler) use ($pageCrawl) {
            foreach ($crawler->filter('a[class="titleLink"]') as $anchor) {
                $closest = app(DomClosest::class);
                /** @var DOMNode $anchor */
                /** @var Crawler $parentNode */
                $parentNode = $closest->closest($anchor, 'h2, h4');
                $parentClass = $parentNode->getNode(0);
                if ($parentClass !== null) {
                    /** @var DOMElement $parentClass */
                    $tocClass = $parentClass->getAttribute('class');
                    if ($tocClass === 'title articleTitle' && !str_starts_with(strtolower(trim($parentClass->textContent)), 'division')) {
                        $parentClass->setAttribute('class', 'title partTitle');
                    }
                }
            }
            $toc = [];
            $prevLevel = 0;
            foreach ($crawler->filter('a[class="titleLink"]') as $anchor) {
                $closest = app(DomClosest::class);
                /** @var DOMNode $anchor */
                /** @var Crawler $parentNode */
                $parentNode = $closest->closest($anchor, 'h2, h4');
                $parentClass = $parentNode->getNode(0);
                if ($parentClass !== null) {
                    /** @var DOMElement $parentClass */
                    $tocClass = $parentClass->getAttribute('class');

                    $hierarchies = [
                        'divisionTitle' => 1,
                        'chapterTitle' => 2,
                        'partTitle' => 3,
                        'articleTitle' => 4,
                        'sectionTitle' => 5,
                    ];

                    $level = 0;
                    $resetLevel = 0;

                    if ($tocClass) {
                        // Use a regular expression to match the class name
                        preg_match('/(divisionTitle|partTitle|chapterTitle|articleTitle|sectionTitle)/', $tocClass, $matches);
                        if ($matches) {
                            // Determine the level based on the matched class name
                            $level = $hierarchies[$matches[0]] ?? 0;

                            // Adjust the level based on the previous hierarchy
                            if ($prevLevel > 0 && $level > 0 && $prevLevel < $level) {
                                $level = $prevLevel + 1;
                            }
                            if ($matches[0] === 'sectionTitle') {
                                $level = $prevLevel + 1;
                                $resetLevel = $level - 1;
                            }
                            $prevLevel = $matches[0] === 'sectionTitle' ? $resetLevel : $level;
                        }
                    }
                    $url = $pageCrawl->pageUrl->url;
                    /** @var DOMElement $anchor */
                    $href = $anchor->getAttribute('href');
                    $tocId = Str::of($href)->replace('#', '');
                    $label = $anchor->textContent ?? '';
                    $anchor->setAttribute('id', $tocId);
                    $toc[] = new TocItemDraft([
                        'source_url' => $href,
                        'label' => trim($label),
                        'source_unique_id' => $href,
                        'href' => $href,
                        'level' => $level,
                    ]);
                }
            }
            $this->arachno->captureTocFromDraftArray($pageCrawl, $toc);

            return $crawler;
        };
        $this->arachno->capture(
            $pageCrawl,
            $bodyQueries,
            $headQueries,
            preStoreCallable: $preStore,
        );
    }

    /**
     * The config that is implemented by the crawler.
     *
     * @return array<string, mixed>
     */
    public function getCrawlerSettings(): array
    {
        return [
            'slug' => 'us-ecode-360-ordinances',
            'throttle_requests' => 500,
            'start_urls' => $this->getEcode360StartUrls(),
        ];
    }

    /**
     * @codeCoverageIgnore
     *
     * @return array<string,array<mixed>>
     */
    public function getEcode360StartUrls(): array
    {
        $arr = [
            'https://ecode360.com/laws/FO3807',
            'https://ecode360.com/laws/NE0870',
            'https://ecode360.com/laws/BA2043',
            'https://ecode360.com/laws/CH2673',
            'https://ecode360.com/laws/MA2384',
            'https://ecode360.com/laws/MO2400',
            'https://ecode360.com/laws/KA6368',
            'https://ecode360.com/laws/SU0867',
            'https://ecode360.com/laws/IS0324',
            'https://ecode360.com/laws/EA2319',
            'https://ecode360.com/laws/HA1391',
            'https://ecode360.com/laws/MA2997',
            'https://ecode360.com/laws/MI1063',
            'https://ecode360.com/laws/UP5026',
            'https://ecode360.com/laws/VE5027',
            'https://ecode360.com/laws/HU4937',
            'https://ecode360.com/laws/LA4953',
            'https://ecode360.com/laws/MI4968',
            'https://ecode360.com/laws/SA4999',
            'https://ecode360.com/laws/SA5006',
        ];
        $codeLinks = [
            'https://ecode360.com/FO3807',
            'https://ecode360.com/NE0870',
            'https://ecode360.com/BA2043',
            'https://ecode360.com/CH2673',
            'https://ecode360.com/MA2384',
            'https://ecode360.com/MO2400',
            'https://ecode360.com/KA6368',
            'https://ecode360.com/SU0867',
            'https://ecode360.com/IS0324',
            'https://ecode360.com/EA2319',
            'https://ecode360.com/HA1391',
            'https://ecode360.com/MA2997',
            'https://ecode360.com/MI1063',
        ];
        $out = [];
        foreach ($arr as $url) {
            $out[] = new UrlFrontierLink(['url' => $url]);
        }

        $codeArr = [];
        foreach ($codeLinks as $url) {
            $codeArr[] = new UrlFrontierLink(['url' => $url]);
        }

        return [
            'type_' . CrawlType::FOR_UPDATES->value => $out,
            'type_' . CrawlType::FULL_CATALOGUE->value => $codeArr,
        ];
    }

    /**
     * @codeCoverageIgnore
     *
     * @return array<string, array<string,mixed>>
     */
    protected function getEcode360LocationsAndTitles(): array
    {
        return [
            'FO3807' => ['location_id' => 7448, 'title' => 'Fox Crossing, WI'],
            'NE0870' => ['location_id' => 52364, 'title' => 'New Milford'],
            'BA2043' => ['location_id' => 74236, 'title' => 'Barnstable'],
            'CH2673' => ['location_id' => 63127, 'title' => 'Cherry Hill'],
            'MA2384' => ['location_id' => 76071, 'title' => 'Manheim City'],
            'MO2400' => ['location_id' => 76075, 'title' => 'Borough of Mount Joy'],
            'KA6368' => ['location_id' => 51110, 'title' => 'Katy City'],
            'SU0867' => ['location_id' => 4278, 'title' => 'Suffolk County, NY'],
            'IS0324' => ['location_id' => 64562, 'title' => 'Township of Islip'],
            'EA2319' => ['location_id' => 76060, 'title' => 'East Hempfield Township'],
            'HA1391' => ['location_id' => 75589, 'title' => 'City of Harrisburg, PA'],
            'MA2997' => ['location_id' => 76072, 'title' => 'Township of Manheim'],
            'MI1063' => ['location_id' => 75599, 'title' => 'Borough of Middletown'],
            'UP5026' => ['location_id' => 69162, 'title' => 'City of Upland, CA'],
            'VE5027' => ['location_id' => 69055, 'title' => 'City of Vernon, CA'],
            'HU4937' => ['location_id' => 69112, 'title' => 'City of Huntington Beach, CA'],
            'LA4953' => ['location_id' => 69109, 'title' => 'City of Laguna Beach'],
            'MI4968' => ['location_id' => 69069, 'title' => 'Mill Valley'],
            'SA4999' => ['location_id' => 2618, 'title' => 'Sacramento County'],
            'SA5006' => ['location_id' => 21009, 'title' => 'City of Santa Barbara'],
        ];
    }
}
