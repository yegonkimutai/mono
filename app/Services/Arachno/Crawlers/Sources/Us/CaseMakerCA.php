<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerCA extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'CA';
}
