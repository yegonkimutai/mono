<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerOR extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'OR';
}
