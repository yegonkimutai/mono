<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

use App\Enums\Arachno\CrawlType;
use App\Models\Arachno\UrlFrontierLink;

/**
 * @codeCoverageIgnore
 * slug: us-casemaker-ut
 * title: USA, Utah Administrative Code
 * url: https://fc7.fastcase.com/outline/UT/476?docUid=571756153
 */
class CaseMakerUT extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'UT';

    /**
     * {@inheritDoc}
     */
    public function getCrawlerSettings(): array
    {
        return [
            'slug' => 'us-casemaker-ut',
            'throttle_requests' => 200,
            'start_urls' => [
                'type_' . CrawlType::FULL_CATALOGUE->value => [
                    new UrlFrontierLink([
                        'url' => 'https://fc7.fastcase.com/outline/UT/476?docUid=571756153',
                    ]),
                ],
            ],
        ];
    }
}
