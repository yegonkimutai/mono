<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerIL extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'IL';
}
