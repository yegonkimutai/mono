<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerMN extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'MN';
}
