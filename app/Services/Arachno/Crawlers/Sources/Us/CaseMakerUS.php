<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerUS extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'US';
}
