<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerCO extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'CO';
}
