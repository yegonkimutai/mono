<?php

namespace App\Services\Arachno\Crawlers\Sources\Us;

/**
 * @codeCoverageIgnore
 */
class CaseMakerPA extends AbstractCaseMakerConfig
{
    protected string $jurisdictionCode = 'PA';
}
