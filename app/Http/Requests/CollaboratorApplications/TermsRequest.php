<?php

namespace App\Http\Requests\CollaboratorApplications;

use Illuminate\Foundation\Http\FormRequest;

class TermsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed> $rules
     */
    public function rules()
    {
        return [
            'process_personal_data' => ['required'],
            'allows_communication' => ['nullable'],
        ];
    }
}
