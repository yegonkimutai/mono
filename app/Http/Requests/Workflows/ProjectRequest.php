<?php

namespace App\Http\Requests\Workflows;

use App\Models\Arachno\Source;
use App\Models\Auth\User;
use App\Models\Collaborators\ProductionPod;
use App\Models\Customer\Organisation;
use App\Models\Geonames\Location;
use App\Models\Ontology\LegalDomain;
use App\Models\Workflows\Board;
use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string,mixed> $rules
     */
    public function rules(): array
    {
        $rules = [
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'start_date' => 'nullable|date',
            'due_date' => 'nullable|date|after_or_equal:start_date',
            'context_due_date' => 'nullable|date|after_or_equal:start_date',
            'actions_due_date' => 'nullable|date|after_or_equal:start_date',
            'organisation_id' => 'nullable|exists:' . (new Organisation())->getTable() . ',id',
            'owner_id' => 'nullable|exists:' . (new User())->getTable() . ',id',
            'board_id' => 'nullable|exists:' . (new Board())->getTable() . ',id',
            'production_pod_id' => 'nullable|exists:' . (new ProductionPod())->getTable() . ',id',
            'location_id' => 'nullable|exists:' . (new Location())->getTable() . ',id',
            'language_code' => 'nullable|string|max:10',
            'domains' => 'nullable|array',
            'domains.*' => 'exists:' . (new LegalDomain())->getTable() . ',id',
            'sources' => 'nullable|array',
            'sources.*' => 'exists:' . (new Source())->getTable() . ',id',
            'tracking_specialists' => 'nullable|array',
            'tracking_specialists.*' => 'exists:' . (new User())->getTable() . ',id',
        ];

        return $rules;
    }
}
