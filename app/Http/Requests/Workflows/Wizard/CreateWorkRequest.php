<?php

namespace App\Http\Requests\Workflows\Wizard;

use App\Http\Requests\Corpus\WorkRequest;

class CreateWorkRequest extends WorkRequest
{
}
