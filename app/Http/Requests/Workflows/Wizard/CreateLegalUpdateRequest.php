<?php

namespace App\Http\Requests\Workflows\Wizard;

use App\Http\Requests\Notify\LegalUpdateRequest;

class CreateLegalUpdateRequest extends LegalUpdateRequest
{
}
