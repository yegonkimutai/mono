<?php

namespace App\Http\Controllers\Actions\My;

use App\Enums\Auth\UserActivityType;
use App\Events\Auth\UserActivity\FilteredResource;
use App\Events\Auth\UserActivity\GenericActivity;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\DecodesHashids;
use App\Models\Auth\User;
use App\Models\Tasks\Task;
use App\Services\Customer\ActiveLibryosManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Inertia\Response;

class ActionsTaskController extends Controller
{
    use DecodesHashids;

    public function index(Request $request, string $view): View|Response
    {
        /** @var View|Response */
        return match ($view) {
            'calendar' => $this->renderCalendar($request),
            default => inertia('Actions/My/Task/IndexPage', ['active' => $view]),
        };
    }

    /**
     * @codeCoverageIgnore
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    protected function renderCalendar(Request $request): View
    {
        /** @var User $user */
        $user = $request->user();
        $organisation = $libryo = null;
        $manager = app(ActiveLibryosManager::class);
        if ($libryo = $manager->getActive($user)) {
            $query = Task::forLibryo($libryo);
        } else {
            $organisation = $manager->getActiveOrganisation();
            $query = Task::forOrganisationUserAccess($organisation, $user);
        }

        $type = $request->hasAny(['show-month', 'show-year']) ? UserActivityType::changedCalendarDates() : UserActivityType::viewedCalendar();
        event(new GenericActivity($user, $type, null, $libryo, $organisation));

        if ($request->get('search')) {
            event(new GenericActivity($user, UserActivityType::searchedTasks(), null, $libryo, $organisation));
        }

        $filtered = $request->only(['statuses', 'type', 'priority', 'assignee', 'archived', 'overdue', 'project']);

        if (!empty($filtered)) {
            event(new FilteredResource($user, UserActivityType::filteredTasks(), $filtered, $libryo, $organisation));
        }

        $month = $request->get('show-month', now()->month);
        $month = (int) $month;
        $month = $month > 12 || $month < 1 ? now()->month : $month;

        $year = $request->get('show-year', now()->year);
        $year = (int) $year;
        $year = $year > 3000 || $year < 2000 ? now()->year : $year;

        return view('pages.actions.my.action-area.tasks.calendar', [
            'filters' => $request->all(),
            'query' => $query->with(['assignee', 'project']),
            'showMonth' => $month,
            'showYear' => $year,
        ]);
    }

    public function show(string $task): View
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var Task $task */
        $task = $this->decodeHash($task, Task::class);
        $this->authorize('view', $task);

        $task->load(['assignee', 'author', 'project', 'watchers', 'previous', 'next', 'libryo:id,title']);

        // @codeCoverageIgnoreStart
        if ($task->isCloned()) {
            $task->load(['taskable.refTitleText']);
        }
        // @codeCoverageIgnoreEnd

        /** @var View */
        return view('pages.actions.my.action-area.tasks.show', ['task' => $task, 'user' => $user]);
    }
}
