<?php

namespace App\Http\Controllers\Corpus\Collaborate;

use App\Models\Corpus\CatalogueDoc;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DocForCatalogueDocController extends DocController
{
    /** @var bool */
    protected bool $searchable = false;

    protected static function forResource(): ?string
    {
        return CatalogueDoc::class;
    }

    /**
     * Get base resource route which will be added the suffix actions.
     *
     * @return string
     */
    protected static function resourceRoute(): string
    {
        return 'collaborate.corpus.catalogue-docs.docs';
    }

    /**
     * @codeCoverageIgnore
     * Get form request to be used when validating the input.
     *
     * @return string
     */
    protected static function resourceFormRequest(): string
    {
        return '';
    }

    /**
     * Generate a new query builder instance for the resource.
     *
     * @param Request $request
     *
     * @return Builder
     */
    protected function baseQuery(Request $request): Builder
    {
        /** @var CatalogueDoc $catalogueDoc */
        $catalogueDoc = $this->getForResource();

        /** @var Builder */
        return parent::baseQuery($request)
            ->whereRelation('catalogueDoc', fn ($q) => $q->whereKey($catalogueDoc->id));
    }

    /**
     * {@inheritDoc}
     */
    protected function resourceRouteParams(): array
    {
        /** @var Request $request */
        $request = request();

        return [
            'catalogueDoc' => $request->route('catalogueDoc'),
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function indexViewData(Request $request): array
    {
        /** @var CatalogueDoc $catalogueDoc */
        $catalogueDoc = $this->getForResource();
        $catalogueDoc->load(['crawler']);

        return [
            'preCreateButtonView' => 'partials.corpus.catalogue-doc.fetch-doc-button',
            'catalogueDoc' => $catalogueDoc,
        ];
    }
}
