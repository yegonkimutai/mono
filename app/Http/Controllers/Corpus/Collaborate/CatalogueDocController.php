<?php

namespace App\Http\Controllers\Corpus\Collaborate;

use App\Contracts\Http\ResourceAction;
use App\Http\Controllers\Abstracts\Collaborate\CollaborateController;
use App\Http\Controllers\Traits\HasShowAction;
use App\Http\ResourceActions\Corpus\FetchCatalogueDocs;
use App\Models\Corpus\CatalogueDoc;
use App\Traits\Arachno\UsesSourceCategoryFilter;
use App\Traits\Arachno\UsesSourceFilter;
use App\Traits\Geonames\UsesJurisdictionFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\View\ComponentAttributeBag;

class CatalogueDocController extends CollaborateController
{
    use HasShowAction;
    use UsesSourceFilter;
    use UsesSourceCategoryFilter;
    use UsesJurisdictionFilter;

    /** @var string */
    protected string $sortBy = 'id';

    /** @var string */
    protected string $sortDirection = 'desc';

    /** @var bool */
    protected bool $withCreate = false;

    /** @var bool */
    protected bool $withDelete = false;

    /** @var bool */
    protected bool $withUpdate = false;

    /**
     * Get the class to be used for the CRUD operations.
     *
     * @return string
     */
    protected static function resource(): string
    {
        return CatalogueDoc::class;
    }

    /**
     * Get base resource route which will be added the suffix actions.
     *
     * @return string
     */
    protected static function resourceRoute(): string
    {
        return 'collaborate.corpus.catalogue-docs';
    }

    /**
     * @codeCoverageIgnore
     * Get form request to be used when validating the input.
     *
     * @return string
     */
    protected static function resourceFormRequest(): string
    {
        return '';
    }

    /**
     * @return array<string, mixed>
     */
    protected static function indexColumns(): array
    {
        return [
            'title' => fn ($row) => static::renderPartial($row, 'title-column'),
            'docs' => fn ($row) => static::renderPartial($row, 'docs-column'),
            'work' => fn ($row) => static::renderPartial($row, 'work-column'),
            'source' => fn ($row) => $row->source?->title . ($row->sourceCategories->isEmpty() ? '' : ' / ' . $row->sourceCategories->pluck('title')->implode(', ')),
            'link' => fn ($row) => static::renderPartial($row, 'link-column'),
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function indexViewData(Request $request): array
    {
        return [
            'headings' => false,
            'fluid' => true,
            'paginate' => 50,
        ];
    }

    /**
     * Generate a new query builder instance for the resource.
     *
     * @param Request $request
     *
     * @return Builder
     */
    protected function baseQuery(Request $request): Builder
    {
        /** @var Builder */
        return parent::baseQuery($request)
            ->with(['source', 'work', 'crawler', 'sourceCategories'])
            ->withCount(['docs', 'sourceCategories'])
            ->orderBy('id', 'desc');
    }

    /**
     * Get the resource filters.
     *
     * @return array<string, array<string, mixed>>
     */
    protected function resourceFilters(): array
    {
        return [
            'jurisdiction' => $this->getJurisdictionFilter(),
            'source' => $this->getSourceFilter(),
            'source_categories' => $this->getSourceCategoryFilter(),
            'failed-fetch' => [
                'label' => __('corpus.catalogue_doc.failed_fetch'),
                'render' => fn () => view('partials.ui.collaborate.input-filter', [
                    'attributes' => new ComponentAttributeBag([
                        'label' => __('corpus.catalogue_doc.failed_fetch'),
                        'value' => $this->getFilterValue('failed-fetch'),
                        'name' => 'failed-fetch',
                        'type' => 'checkbox',
                    ]),
                ]),
                'value' => fn ($value) => $value === 'true' ? __('interface.yes') : '',
            ],
        ];
    }

    /**
     * Get the resource actions.
     *
     * @return array<int, ResourceAction>
     */
    protected static function resourceActions(): array
    {
        return [
            new FetchCatalogueDocs(),
        ];
    }
}
