<?php

namespace App\Http\Controllers\Corpus\Collaborate;

use App\Http\Controllers\Controller;
use App\Stores\Corpus\ContentResourceStore;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Tonysm\TurboLaravel\Http\PendingTurboStreamResponse;

class ContentResourceUploadController extends Controller
{
    public function __construct(
        protected ContentResourceStore $contentResourceStore,
    ) {
    }

    public function upload(Request $request): PendingTurboStreamResponse
    {
        /** @var UploadedFile $file */
        $file = $request->file('file');

        $mimeType = $file->getClientMimeType();
        $contentResource = $this->contentResourceStore
            ->storeResource($file->getContent(), $mimeType);

        $url = $this->contentResourceStore->getLinkForResource($contentResource);

        return singleTurboStreamResponse('content_resource_uploader_url', 'update')
            ->view('partials.corpus.collaborate.content-resource.content-resource-url', [
                'url' => $url,
            ]);
    }
}
