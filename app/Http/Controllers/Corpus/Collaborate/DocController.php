<?php

namespace App\Http\Controllers\Corpus\Collaborate;

use App\Actions\Corpus\Work\CreateFromDoc;
use App\Http\Controllers\Abstracts\Collaborate\CollaborateController;
use App\Http\Controllers\Traits\HasShowAction;
use App\Models\Corpus\Doc;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DocController extends CollaborateController
{
    use HasShowAction;

    /** @var string */
    protected string $sortBy = 'id';

    /** @var string */
    protected string $sortDirection = 'desc';

    /** @var bool */
    protected bool $withCreate = false;

    /** @var bool */
    protected bool $withDelete = false;

    /** @var bool */
    protected bool $withUpdate = false;

    /**
     * Get the class to be used for the CRUD operations.
     *
     * @return string
     */
    protected static function resource(): string
    {
        return Doc::class;
    }

    /**
     * Get base resource route which will be added the suffix actions.
     *
     * @return string
     */
    protected static function resourceRoute(): string
    {
        return 'collaborate.corpus.docs';
    }

    /**
     * @codeCoverageIgnore
     * Get form request to be used when validating the input.
     *
     * @return string
     */
    protected static function resourceFormRequest(): string
    {
        return '';
    }

    /**
     * @return array<string, mixed>
     */
    protected static function indexColumns(): array
    {
        return [
            'id' => fn ($row) => static::renderPartial($row, 'id-column'),
            'title' => fn ($row) => static::renderPartial($row, 'title-column'),
            'preview' => fn ($row) => static::renderPartial($row, 'preview-column'),
        ];
    }

    /**
     * Generate a new query builder instance for the resource.
     *
     * @param Request $request
     *
     * @return Builder
     */
    protected function baseQuery(Request $request): Builder
    {
        /** @var Builder */
        return parent::baseQuery($request)
            ->with(['source', 'legalDomains:id,title', 'primaryLocation:id,flag,title', 'docMeta', 'categories:id,display_label', 'keywords:id,label'])
            ->orderBy('id', 'desc');
    }

    public function preview(Doc $doc): View
    {
        $doc->load('docMeta');

        /** @var View */
        return view('pages.corpus.collaborate.doc.preview', [
            'doc' => $doc,
        ]);
    }

    public function generateWork(Doc $doc): RedirectResponse
    {
        Session::flash('flash.message', __('corpus.doc.new_work_to_be_generated'));

        CreateFromDoc::dispatch($doc->id);

        return back();
    }
}
