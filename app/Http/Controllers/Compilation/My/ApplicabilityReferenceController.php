<?php

namespace App\Http\Controllers\Compilation\My;

use App\Enums\Compilation\ContextQuestionAnswer;
use App\Models\Corpus\Reference;
use App\Models\Customer\Pivots\ContextQuestionLibryo;
use App\Models\Geonames\Location;
use App\Models\Geonames\Pivots\LocationLocation;
use App\Services\Customer\ActiveLibryosManager;
use Illuminate\Http\Response;
use Tonysm\TurboLaravel\Http\PendingTurboStreamResponse;

class ApplicabilityReferenceController
{
    /**
     * Show the applicability pane.
     *
     * @param int $reference
     *
     * @return \Tonysm\TurboLaravel\Http\PendingTurboStreamResponse|\Illuminate\Http\Response
     */
    public function show(int $reference): PendingTurboStreamResponse|Response
    {
        $manager = app(ActiveLibryosManager::class);

        if (!$manager->isSingleMode()) {
            /** @var Response */
            return response('');
        }
        /** @var Reference $reference */
        $reference = Reference::findOrFail($reference);

        /** @var \App\Models\Customer\Libryo $libryo */
        $libryo = $manager->getActive();

        $locations = LocationLocation::where('descendant', $libryo->location_id)
            ->orderBy('depth', 'desc')
            ->pluck('ancestor');

        $fetched = Location::whereIn('id', $locations->all())->pluck('title', 'id');
        $locations = $locations->map(fn ($ancestor) => $fetched->get($ancestor));

        $questions = $reference->contextQuestions()
            ->whereHas('libryos', function ($builder) use ($libryo) {
                $builder->where('id', $libryo->id)
                    ->where((new ContextQuestionLibryo())->qualifyColumn('answer'), ContextQuestionAnswer::yes()->value);
            })
            ->get();

        $categories = $reference->legalDomains()
            ->whereRelation('libryos', 'id', $libryo->id)
            ->get();

        return singleTurboStreamResponse("applicability-for-{$reference->id}")
            ->view('partials.corpus.reference.my.applicability-for-reference', [
                'locations' => $locations,
                'questions' => $questions,
                'categories' => $categories,
                'reference' => $reference,
                'libryo' => $libryo,
            ]);
    }
}
