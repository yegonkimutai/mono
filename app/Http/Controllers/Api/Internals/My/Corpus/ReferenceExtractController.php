<?php

namespace App\Http\Controllers\Api\Internals\My\Corpus;

use App\Http\Resources\Internals\My\Corpus\ReferenceContentExtractResource;
use App\Models\Corpus\Reference;
use App\Models\Corpus\ReferenceContentExtract;
use App\Models\Tasks\Task;
use App\Services\Customer\ActiveLibryosManager;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ReferenceExtractController
{
    /**
     * @param \App\Services\Customer\ActiveLibryosManager $manager
     * @param \App\Models\Corpus\Reference                $reference
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(ActiveLibryosManager $manager, Reference $reference): AnonymousResourceCollection
    {
        $content = ReferenceContentExtract::where('reference_id', $reference->id)
            ->select(['id', 'content'])
            ->addSelect([
                'attached' => Task::whereColumn('reference_content_extract_id', qualify_column(ReferenceContentExtract::class, 'id'))
                    ->forLibryoOrOrganisation($manager->getActive(), $manager->getActiveOrganisation())
                    ->selectRaw('1'),
            ])
            ->withCasts([
                'attached' => 'boolean',
            ])
            ->get(['id', 'content']);

        return ReferenceContentExtractResource::collection($content);
    }
}
