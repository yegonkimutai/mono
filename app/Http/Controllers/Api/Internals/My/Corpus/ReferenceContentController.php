<?php

namespace App\Http\Controllers\Api\Internals\My\Corpus;

use App\Events\Auth\UserActivity\UserTranslatedContent;
use App\Http\Resources\Internals\My\Corpus\ReferenceContentResource;
use App\Models\Auth\User;
use App\Models\Corpus\Reference;
use App\Models\Corpus\ReferenceContent;
use App\Models\Corpus\Work;
use App\Repositories\Auth\UserActivityRepository;
use App\Services\Customer\ActiveLibryosManager;
use App\Services\Translation\ModelTranslator;
use App\Support\Languages;
use Illuminate\Support\Facades\Auth;

class ReferenceContentController
{
    /**
     * Get the content for the given reference.
     *
     * @param \App\Models\Corpus\Reference $reference
     * @param string|null                  $language
     *
     * @return ReferenceContentResource
     */
    public function show(Reference $reference, ?string $language = null): ReferenceContentResource
    {
        $content = ReferenceContent::where('reference_id', $reference->id)
            ->select(['cached_content'])
            ->firstOrFail();

        if ($language) {
            $content->cached_content = $this->translate($reference, $content->cached_content, $language);
        }

        return new ReferenceContentResource($content);
    }

    /**
     * Translate the given reference content.
     *
     * @param \App\Models\Corpus\Reference $reference
     * @param string|null                  $content
     * @param string                       $language
     *
     * @return string
     */
    public function translate(Reference $reference, ?string $content, string $language): string
    {
        /** @var Work $work */
        $work = Work::where('id', $reference->work_id)->select(['language_code'])->first();

        $workLanguage = Languages::$alpha3To2[$work->language_code] ?? 'en';

        if ($language !== $workLanguage && !empty($content)) {
            $this->logTranslated($reference);
            $content = app(ModelTranslator::class)->translate($content, $workLanguage, $language);
        }

        return $content ?? '';
    }

    /**
     * Log the user activity.
     *
     * @param \App\Models\Corpus\Reference $reference
     *
     * @return void
     */
    public function logTranslated(Reference $reference): void
    {
        $repo = app(UserActivityRepository::class);
        /** @var User $user */
        $user = Auth::user();
        $manager = app(ActiveLibryosManager::class);
        $event = new UserTranslatedContent($user, $reference, $manager->getActive(), $manager->getActiveOrganisation());
        $repo->addUserActivityEvent($event);
    }
}
