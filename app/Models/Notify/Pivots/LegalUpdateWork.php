<?php

namespace App\Models\Notify\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperLegalUpdateWork
 */
class LegalUpdateWork extends Pivot
{
}
