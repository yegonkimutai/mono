<?php

namespace App\Models\Notify\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperNotificationActionUpdateCandidate
 */
class NotificationActionUpdateCandidate extends Pivot
{
}
