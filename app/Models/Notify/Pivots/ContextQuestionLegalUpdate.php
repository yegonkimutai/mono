<?php

namespace App\Models\Notify\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperContextQuestionLegalUpdate
 */
class ContextQuestionLegalUpdate extends Pivot
{
}
