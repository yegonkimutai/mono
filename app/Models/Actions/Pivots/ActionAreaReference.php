<?php

namespace App\Models\Actions\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperActionAreaReference
 */
class ActionAreaReference extends Pivot
{
}
