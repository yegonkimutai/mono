<?php

namespace App\Models\Actions\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperActionAreaReferenceDraft
 */
class ActionAreaReferenceDraft extends Pivot
{
}
