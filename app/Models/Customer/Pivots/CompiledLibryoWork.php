<?php

namespace App\Models\Customer\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperCompiledLibryoWork
 */
class CompiledLibryoWork extends Pivot
{
}
