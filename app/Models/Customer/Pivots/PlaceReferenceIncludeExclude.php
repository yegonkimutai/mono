<?php

namespace App\Models\Customer\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperPlaceReferenceIncludeExclude
 */
class PlaceReferenceIncludeExclude extends Pivot
{
}
