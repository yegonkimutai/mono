<?php

namespace App\Models\Customer\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperCompiledLibryoReference
 */
class CompiledLibryoReference extends Pivot
{
}
