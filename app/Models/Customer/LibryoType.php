<?php

namespace App\Models\Customer;

use App\Models\AbstractModel;

/**
 * @mixin IdeHelperLibryoType
 */
class LibryoType extends AbstractModel
{
    protected $table = 'place_types';
}
