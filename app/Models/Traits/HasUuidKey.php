<?php

namespace App\Models\Traits;

use Illuminate\Support\Str;

trait HasUuidKey
{
    /**
     * Boot up the trait.
     *
     * @return void
     */
    protected static function bootHasUuidKey(): void
    {
        static::creating(function ($model) {
            $key = $model->getKeyName();
            if (empty($model->{$key})) {
                $model->{$key} = Str::uuid()->toString();
            }
        });
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }
}
