<?php

namespace App\Models\Compilation\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperLibraryLibrary
 */
class LibraryLibrary extends Pivot
{
}
