<?php

namespace App\Models\Ontology;

use App\Models\AbstractModel;

/**
 * @mixin IdeHelperCategoryType
 */
class CategoryType extends AbstractModel
{
    protected $table = 'corpus_category_types';
}
