<?php

namespace App\Models\Ontology\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperLegalDomainLocation
 */
class LegalDomainLocation extends Pivot
{
}
