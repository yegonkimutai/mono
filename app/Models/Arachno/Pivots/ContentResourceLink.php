<?php

namespace App\Models\Arachno\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperContentResourceLink
 */
class ContentResourceLink extends Pivot
{
}
