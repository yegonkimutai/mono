<?php

namespace App\Models\Arachno\Pivots;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @mixin IdeHelperLocationSource
 */
class LocationSource extends Pivot
{
}
