<?php

namespace App\Enums\Requirements;

enum RefRequirementChangeStatus: int
{
    case ADD = 1;
    case REMOVE = 0;
}
