<?php

namespace App\Enums\Workflows;

enum OnToDoValidationType: string
{
    case AUTO_ARCHIVE = 'auto_archive';
}
