<?php

namespace App\Enums\Ontology;

enum CategoryType: int
{
    case SUBJECT = 1;
    case ACTIVITY = 2;
    case ECONOMIC = 3;
    case TOPICS = 4;
    case CONTROL = 5;
}
