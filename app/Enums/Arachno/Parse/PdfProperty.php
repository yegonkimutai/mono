<?php

namespace App\Enums\Arachno\Parse;

enum PdfProperty
{
    case TITLE;
    case SUBJECT;
    case KEYWORDS;
    case CREATOR;
    case PRODUCER;
    case CREATION_DATE;
}
