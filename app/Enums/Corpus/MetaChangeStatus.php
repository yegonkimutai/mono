<?php

namespace App\Enums\Corpus;

enum MetaChangeStatus: int
{
    case REMOVE = 0;
    case ADD = 1;
}
