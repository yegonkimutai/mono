<?php

namespace App\Contracts\Arachno;

use App\Models\Arachno\UrlFrontierLink;

/**
 * @method void replicate(array $except = null)
 *
 * @mixin UrlFrontierLink
 */
interface UrlFrontierLinkInterface
{
}
