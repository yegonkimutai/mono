<?php

namespace App\Stores\Corpus;

use App\Stores\Traits\AttachesDetaches;

class ReferenceRequirementsCollectionStore
{
    use AttachesDetaches;
}
