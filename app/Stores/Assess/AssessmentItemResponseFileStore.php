<?php

namespace App\Stores\Assess;

use App\Stores\Traits\AttachesDetaches;

class AssessmentItemResponseFileStore
{
    use AttachesDetaches;
}
