<?php

namespace Database\Factories\Arachno;

use App\Models\Arachno\Crawl;
use Illuminate\Database\Eloquent\Factories\Factory;

class CrawlFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Crawl::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
        ];
    }
}
