<?php

namespace Database\Factories\Collaborate;

use App\Models\Collaborators\ProfileDocument;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ProfileDocument>
 */
class ProfileDocumentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
        ];
    }
}
