<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('url_frontier_links', function (Blueprint $table) {
            $table->string('url', 2000)->change();
            $table->string('referer', 2000)->change();
        });

        Schema::table('content_caches', function (Blueprint $table) {
            $table->string('url', 2000)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('url_frontier_links', function (Blueprint $table) {
            $table->string('url', 511)->change();
            $table->string('referer', 511)->change();
        });

        Schema::table('content_caches', function (Blueprint $table) {
            $table->string('url', 511)->change();
        });
    }
};
