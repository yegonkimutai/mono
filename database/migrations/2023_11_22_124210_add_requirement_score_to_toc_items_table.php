<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('toc_items', function (Blueprint $table) {
            $table->float('requirement_score', 8, 4)->after('content_hash')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('toc_items', function (Blueprint $table) {
            $table->dropColumn(['requirement_score']);
        });
    }
};
