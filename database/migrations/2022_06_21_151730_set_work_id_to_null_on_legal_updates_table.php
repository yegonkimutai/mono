<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('corpus_legal_updates', function (Blueprint $table) {
            $table->unsignedInteger('work_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corpus_legal_updates', function (Blueprint $table) {
            $table->unsignedInteger('work_id')->nullable(false)->change();
        });
    }
};
